package dev.byto.tmdb.constants

object UrlConstants {
    /**
     * TMDB links
     */
    const val TMDB_BASE_URL = "https://api.themoviedb.org/3/"


    const val TMDB_POSTER_SIZE_185 = "https://image.tmdb.org/t/p/w185"
    const val TMDB_POSTER_SIZE_500 = "https://image.tmdb.org/t/p/w500"
    const val TMDB_BACKDROP_SIZE_780 = "https://image.tmdb.org/t/p/w780"
    const val TMDB_BACKDROP_SIZE_1280 = "https://image.tmdb.org/t/p/w1280"
    const val TMDB_PROFILE_SIZE_185 = "https://image.tmdb.org/t/p/w185"

    /**
     * YouTube links
     */
    const val YOUTUBE_IMAGE_LINK = "https://img.youtube.com/vi/"
    const val YOUTUBE_VDN_LINK = "vnd.youtube:"
    const val YOUTUBE_WEB__LINK = "https://www.youtube.com/watch?v="
    const val YOUTUBE_BASE_URL = "https://www.googleapis.com/youtube/v3/"
}