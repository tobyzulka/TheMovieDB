package dev.byto.tmdb.constants

import java.util.*

object AppConfig {
    var APP_LOCALE: Locale = Locale.getDefault()
    var REGION = "en-US" // default en-US

    val AVAILABLE_LANGUAGES: List<Locale> = listOf(
        Locale.ENGLISH,
        Locale("en", "US"),
        Locale("id", "ID")
    )
}
