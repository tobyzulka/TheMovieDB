package dev.byto.tmdb.constants

object DiscoverFilters {
    const val SORT_BY = "sort_by"
    const val WITH_GENRES = "without_genres"
    const val VOTE_AVERAGE = "vote_average"
    const val RELEASE_DATE = "release_date"
    const val YEAR = "year"
    const val INCLUDE_ADULT = "include_adult"

    /**
     * [TYPE_LTE] - less than or equal to (maximum)
     * [TYPE_GTE] - greater than or equal to (minimum)
     */
    const val TYPE_LTE = "lte"
    const val TYPE_GTE = "gte"
}
