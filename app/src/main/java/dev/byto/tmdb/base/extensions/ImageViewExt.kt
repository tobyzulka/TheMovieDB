package dev.byto.tmdb.base.extensions

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Outline
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import androidx.core.content.ContextCompat
import coil.load
import dev.byto.tmdb.R

fun ImageView.displayImageWithCenterInside(
    url: String?,
    placeholder: Int = R.drawable.ic_person
) {
    load(url) {
        placeholder(placeholder)
        error(placeholder)
    }
}

fun ImageView.displayImageWithCenterCrop(
    url: String?,
    placeholder: Int = R.drawable.placeholder_transparent
) {
    scaleType = ImageView.ScaleType.CENTER_CROP

    load(url) {
        placeholder(placeholder)
        error(placeholder)
    }
}

fun ImageView.setIndicatorColor(voteAverage: Double) {
    val selectedColor: Int = when {
        voteAverage >= 8.0 -> {
            R.color.emerald
        }

        voteAverage > 6.0 && voteAverage < 8.0 -> {
            R.color.colorAccent
        }

        else -> {
            R.color.sunset_orange
        }
    }

    this.setColorFilter(
        ContextCompat.getColor(
            this.context,
            selectedColor
        )
    )
}
