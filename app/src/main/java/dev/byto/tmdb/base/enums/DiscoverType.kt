package dev.byto.tmdb.base.enums

enum class DiscoverType {
    MOVIES
}

fun DiscoverType.isMoviesType() = this == DiscoverType.MOVIES

fun DiscoverType.isMoviesTypeWithAction(action: () -> Unit) {
    if (isMoviesType()) action()
}

