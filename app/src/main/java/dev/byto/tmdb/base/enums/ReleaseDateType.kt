package dev.byto.tmdb.base.enums

enum class ReleaseDateType {
    ANY, BETWEEN_DATES, ONE_YEAR
}
