package dev.byto.tmdb.base.enums

enum class SortFeature(val filtersQueryName: String) {
    /**
     * [ASCENDING] - ascending
     * [DESCENDING] - descending
     */
    ASCENDING("asc"),
    DESCENDING("desc")
}
