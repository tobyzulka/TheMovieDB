package dev.byto.tmdb.base.enums

enum class MovieCollectionType {
    POPULAR,
    TOP_RATED,
    UPCOMING,
    NOW_PLAYING
}
