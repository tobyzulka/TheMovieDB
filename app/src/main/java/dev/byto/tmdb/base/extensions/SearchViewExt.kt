package dev.byto.tmdb.base.extensions

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Lifecycle
import dev.byto.tmdb.utils.DebounceQueryTextListener
import timber.log.Timber

fun SearchView.onQueryTextChange(lifecycle: Lifecycle, action: (String) -> Unit) {
    this.setOnQueryTextListener(
        DebounceQueryTextListener(lifecycle) { newText ->
            newText.let {
                action.invoke(newText ?: "")
                Timber.i(newText)
            }
        }
    )
}
