package dev.byto.tmdb.base.extensions

import com.akexorcist.localizationactivity.ui.LocalizationActivity
import dev.byto.tmdb.constants.AppConfig
import java.util.*

fun LocalizationActivity.updateLanguage(locale: Locale) {
    setLanguage(locale)
    AppConfig.REGION = locale.toLanguageTag()
    AppConfig.APP_LOCALE = locale
}
