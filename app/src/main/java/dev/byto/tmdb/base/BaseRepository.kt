package dev.byto.tmdb.base

import com.orhanobut.logger.Logger
import com.squareup.moshi.Moshi
import dev.byto.tmdb.data.models.error.ErrorResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import java.io.IOException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException

open class BaseRepository {
    suspend fun <T> safeApiCall(
        dispatcher: CoroutineDispatcher,
        apiCall: suspend () -> T
    ): ResultWrapper<T> {
        return withContext(dispatcher) {
            try {
                ResultWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> ResultWrapper.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        val errorResponse = convertErrorBody(throwable)

                        Logger.d("HttpException: $code, ${errorResponse?.statusCode}, ${errorResponse?.statusMessage}")

                        ResultWrapper.GenericError(code, errorResponse)
                    }
                    else -> {
                        Logger.d("GenericError ${throwable.message}")

                        ResultWrapper.GenericError(null, null)
                    }
                }
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
        return try {
            throwable.response()?.errorBody()?.source()?.let {
                val moshiAdapter = Moshi.Builder().build().adapter(ErrorResponse::class.java)
                moshiAdapter.fromJson(it)
            }
        } catch (exception: Exception) {
            null
        }
    }
}
