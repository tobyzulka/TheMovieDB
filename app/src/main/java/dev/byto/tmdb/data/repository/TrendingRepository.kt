package dev.byto.tmdb.data.repository

import dev.byto.tmdb.base.BaseRepository
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class TrendingRepository(private val api: ApiService) : BaseRepository() {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    enum class MediaType(val path: String) {
        ALL("all"), MOVIE("movie")
    }

    enum class TimeWindow(val path: String) {
        DAY("day"), WEEK("week")
    }

    suspend fun getTrendingMovies(
        timeWindow: TimeWindow,
        page: Int?,
        language: String?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.getTrendingMovies(MediaType.MOVIE.path, timeWindow.path, page, language)
        }
    }
}
