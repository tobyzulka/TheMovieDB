package dev.byto.tmdb.data.repository

import dev.byto.tmdb.base.BaseRepository
import dev.byto.tmdb.data.models.entity.Review
import dev.byto.tmdb.data.models.network.ReviewResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class ReviewRepository(private val api: ApiService) : BaseRepository() {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    suspend fun getMovieReviewsById(
        movieId: Int,
        language: String?,
        page: Int?
    ): ResultWrapper<ReviewResponse> {
        return safeApiCall(dispatcher) {
            api.getMovieReviewsById(movieId, language, page)
        }
    }
}
