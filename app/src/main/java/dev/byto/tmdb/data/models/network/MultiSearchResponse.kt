package dev.byto.tmdb.data.models.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MultiSearchResponse(
    @Json(name = "page") val page: Int,
    @Json(name = "results") val results: List<MultiSearchItem>,
    @Json(name = "total_pages") val totalPages: Int,
    @Json(name = "total_results") val totalResults: Int
) {
    @JsonClass(generateAdapter = true)
    data class MultiSearchItem(
        @Json(name = "id") val id: Int,
        @Json(name = "poster_path") val posterPath: String?,
        @Json(name = "profile_path") val profilePath: String?,
        @Json(name = "media_type") val mediaType: String,
        @Json(name = "name") val name: String?,
        @Json(name = "title") val title: String?
    )
}
