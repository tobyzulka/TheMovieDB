package dev.byto.tmdb.data.models.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Credits(
    @Json(name = "cast") val casts: List<Cast>,
    @Json(name = "crew") val crews: List<Crew>
)
