package dev.byto.tmdb.data.models.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import dev.byto.tmdb.data.models.entity.Movie

@JsonClass(generateAdapter = true)
data class MovieResponse(
    @Json(name = "page") val page: Int,
    @Json(name = "results") val results: List<Movie>,
    @Json(name = "total_results") val totalResults: Int,
    @Json(name = "total_pages") val totalPages: Int
)
