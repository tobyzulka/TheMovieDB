package dev.byto.tmdb.data.models.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import dev.byto.tmdb.data.models.entity.Video

@JsonClass(generateAdapter = true)
data class VideosResponse(
    @Json(name = "results") val results: List<Video>
)
