package dev.byto.tmdb.data.repository

import dev.byto.tmdb.base.BaseRepository
import dev.byto.tmdb.data.models.DiscoverFiltersModel
import dev.byto.tmdb.data.models.getFiltersMapForMovies
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class DiscoverRepository(private val api: ApiService) : BaseRepository() {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    suspend fun getDiscoverMovies(
        language: String?,
        page: Int?,
        filtersModel: DiscoverFiltersModel
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.getDiscoverMovies(language, page, filtersModel.getFiltersMapForMovies())
        }
    }
    suspend fun getDiscoverGenreMovies(
        language: String?,
        page: Int?,
        filtersModel: DiscoverFiltersModel
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.getDiscoverMovies(language, page, filtersModel.getFiltersMapForMovies())
        }
    }
}
