package dev.byto.tmdb.data.models

import dev.byto.tmdb.base.enums.DiscoverType
import dev.byto.tmdb.base.enums.SortFeature
import dev.byto.tmdb.base.enums.SortType
import dev.byto.tmdb.constants.DiscoverFilters.INCLUDE_ADULT
import dev.byto.tmdb.constants.DiscoverFilters.RELEASE_DATE
import dev.byto.tmdb.constants.DiscoverFilters.SORT_BY
import dev.byto.tmdb.constants.DiscoverFilters.TYPE_GTE
import dev.byto.tmdb.constants.DiscoverFilters.TYPE_LTE
import dev.byto.tmdb.constants.DiscoverFilters.VOTE_AVERAGE
import dev.byto.tmdb.constants.DiscoverFilters.WITH_GENRES
import dev.byto.tmdb.constants.DiscoverFilters.YEAR
import java.io.Serializable
import java.text.DecimalFormat

data class DiscoverFiltersModel(
    /**
     * [discoverType] Type of elements in search (Movies or TV series)
     */

    var discoverType: DiscoverType = DiscoverType.MOVIES,

    /**
     * Sort type: [sortType]
     * - by date, by polarity, by number of ratings, by average rating
     */
    var sortType: SortType = SortType.BY_POPULARITY,

    /**
     * Sorting Feature: [sortFeature]
     * - descending, ascending
     */
    var sortFeature: SortFeature = SortFeature.DESCENDING,

    /**
     * Filter by year, three options available:
     * - any year - both fields must be null
     * - by year - we use only [minYear]
     * - between years - use both fields ([minYear] and [maxYear])
     */
    var minYear: Int? = null,
    var maxYear: Int? = null,

    /**
     * Filter by average rating ([voteAverageMin] and [voteAverageMax]
     */
    var voteAverageMin: Float? = null,
    var voteAverageMax: Float? = null,

    /**
     * Filter by genre [movieGenresIds]
     */
    var movieGenresIds: List<Int>? = null,

    /**
     * Filter by keywords [keywordsIds]
     */
    var keywordsIds: List<Int>? = null,

    /**
     * Filter by actors [creditsIds] (only for movies)!!!
     */
    var creditsIds: List<Int>? = null,

    /**
     * Include Adult content [includeAdult] (movies only)!!!
     */
    var includeAdult: Boolean? = null,

    ): Serializable {
    /**
     * Method for restoring default values [reset]
     */
    fun reset() {
        sortType = SortType.BY_POPULARITY
        sortFeature = SortFeature.DESCENDING

        minYear = null
        maxYear = null

        voteAverageMin = null
        voteAverageMax = null

        movieGenresIds = null
        keywordsIds = null
        creditsIds = null

        includeAdult = null
    }
}

fun DiscoverFiltersModel.getFiltersMapForMovies(): Map<String, String> {
    val decimalFormat = DecimalFormat("##.#")
    val mapFilters: MutableMap<String, String> = mutableMapOf()

    mapFilters[SORT_BY] = "${sortType.filterQueryName}.${sortFeature.filtersQueryName}"

    includeAdult?.let { mapFilters[INCLUDE_ADULT] = it.toString() }

    voteAverageMin?.let { mapFilters["$VOTE_AVERAGE.$TYPE_GTE"] = decimalFormat.format(it / 10).toString() }
    voteAverageMax?.let { mapFilters["$VOTE_AVERAGE.$TYPE_LTE"] = decimalFormat.format(it / 10).toString() }

    if (minYear != null && maxYear == null) {
        minYear?.let { mapFilters[YEAR] = it.toString() }
    } else {
        minYear?.let { mapFilters["$RELEASE_DATE.$TYPE_GTE"] = "$it-01-01" }
        maxYear?.let { mapFilters["$RELEASE_DATE.$TYPE_LTE"] = "$it-12-31" }
    }

    movieGenresIds?.let { mapFilters[WITH_GENRES] = it.joinToString("|") { id -> id.toString() } }

    return mapFilters
}
