package dev.byto.tmdb.data.repository

import dev.byto.tmdb.base.BaseRepository
import dev.byto.tmdb.data.models.entity.MovieDetails
import dev.byto.tmdb.data.models.entity.MovieImages
import dev.byto.tmdb.data.models.network.GenreResponse
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class MovieRepository(private val api: ApiService) : BaseRepository() {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    suspend fun getPopularMovies(
        language: String?,
        page: Int?,
        region: String?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) { api.getPopularMovies(language, page, region) }
    }

    suspend fun getTopRatedMovies(
        language: String?,
        page: Int?,
        region: String?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) { api.getTopRatedMovies(language, page, region) }
    }

    suspend fun getMovieGenres(language: String?): ResultWrapper<GenreResponse> {
        return safeApiCall(dispatcher) { api.getMovieGenres(language) }
    }

    suspend fun getUpcomingMovies(
        language: String?,
        page: Int?,
        region: String?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.getUpcomingMovies(language, page, region)
        }
    }

    suspend fun getNowPlayingMovies(
        language: String?,
        page: Int?,
        region: String?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.getNowPlayingMovies(language, page, region)
        }
    }

    suspend fun getMovieById(
        movieId: Int,
        language: String?,
        appendToResponse: String?
    ): ResultWrapper<MovieDetails> {
        return safeApiCall(dispatcher) {
            api.getMovieById(movieId, language, appendToResponse)
        }
    }

    suspend fun getMovieImagesById(
        movieId: Int,
        language: String?,
        imageLanguages: String?
    ): ResultWrapper<MovieImages> {
        return safeApiCall(dispatcher) {
            api.getMovieImagesById(movieId, language, imageLanguages)
        }
    }

}
