package dev.byto.tmdb.data.models.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieImages(
    @Json(name="logos") val logos: List<ImageDetails>,
    @Json(name="id") val id: Int,
    @Json(name = "backdrops") val backdrops: List<ImageDetails>,
    @Json(name = "posters") val posters: List<ImageDetails>
)
