package dev.byto.tmdb.data.repository

import dev.byto.tmdb.base.BaseRepository
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.network.MultiSearchResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class SearchRepository(private val api: ApiService) : BaseRepository() {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    suspend fun multiSearch(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?
    ): ResultWrapper<MultiSearchResponse> {
        return safeApiCall(dispatcher) {
            api.multiSearch(language, query, page, includeAdult)
        }
    }

    suspend fun searchMovies(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?,
        region: String?,
        year: Int?,
        primaryReleaseYear: Int?
    ): ResultWrapper<MovieResponse> {
        return safeApiCall(dispatcher) {
            api.searchMovies(
                language,
                query,
                page,
                includeAdult,
                region,
                year,
                primaryReleaseYear
            )
        }
    }
}
