package dev.byto.tmdb.data.models.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.data.models.entity.Review

@JsonClass(generateAdapter = true)
data class ReviewResponse(
    @Json(name = "id") val id: Int,
    @Json(name = "page") val page: Int,
    @Json(name = "results") val results: List<Review>,
    @Json(name = "total_results") val totalResults: Int,
    @Json(name = "total_pages") val totalPages: Int
)
