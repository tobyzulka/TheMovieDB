package dev.byto.tmdb.data.remote

import dev.byto.tmdb.data.models.entity.MovieImages
import dev.byto.tmdb.data.models.entity.MovieDetails
import dev.byto.tmdb.data.models.entity.Review
import dev.byto.tmdb.data.models.network.GenreResponse
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.network.MultiSearchResponse
import dev.byto.tmdb.data.models.network.ReviewResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * All Api services must specific in this interface
 *
 * @Author Toby Zulkarnain.
 */
interface ApiService {

    /**
     * Movies
     */
    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): MovieResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): MovieResponse

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): MovieResponse

    @GET("movie/now_playing")
    suspend fun getNowPlayingMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): MovieResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieById(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String?,
        @Query("append_to_response") appendToResponse: String?,
    ): MovieDetails

    @GET("movie/{movie_id}/images")
    suspend fun getMovieImagesById(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String?,
        @Query("include_image_language") imageLanguages: String?,
    ): MovieImages

    /**
     * Search
     */
    @GET("search/multi")
    suspend fun multiSearch(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("include_adult") includeAdult: Boolean?
    ): MultiSearchResponse

    @GET("search/movie")
    suspend fun searchMovies(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("include_adult") includeAdult: Boolean?,
        @Query("region") region: String?,
        @Query("year") year: Int?,
        @Query("primary_release_year") primaryReleaseYear: Int?
    ): MovieResponse

    /**
     * Genres
     */
    @GET("genre/movie/list")
    suspend fun getMovieGenres(
        @Query("language") language: String?
    ): GenreResponse

    /**
     * Reviews
     */
    @GET("movie/{movie_id}/reviews")
    suspend fun getMovieReviewsById(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String?,
        @Query("page") page: Int?
    ): ReviewResponse

    /**
     * Trending
     */
    @GET("trending/{media_type}/{time_window}")
    suspend fun getTrendingMovies(
        @Path("media_type") mediaType: String,
        @Path("time_window") timeWindow: String,
        @Query("page") page: Int?,
        @Query("language") language: String?
    ): MovieResponse

    /**
     * Discover
     */
    @GET("discover/movie")
    suspend fun getDiscoverMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @QueryMap filters: Map<String, String>
    ): MovieResponse
}