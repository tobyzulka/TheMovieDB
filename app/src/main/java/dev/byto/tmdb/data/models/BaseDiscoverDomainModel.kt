package dev.byto.tmdb.data.models

import dev.byto.tmdb.base.enums.DiscoverType
import dev.byto.tmdb.data.models.entity.Movie
import korlibs.time.DateFormat
import korlibs.time.DateTimeTz

data class BaseDiscoverDomainModel(
    val id: Int,
    val type: DiscoverType,
    val name: String,
    val genres: List<Int>?,
    val posterImage: String?,
    val backdropImage: String?,
    val releaseDate: DateTimeTz?,
    val voteAverage: Double
)

fun Movie.toDiscoverDomainModel() = BaseDiscoverDomainModel(
    id = id,
    type = DiscoverType.MOVIES,
    name = title,
    genres = genreIds,
    posterImage = posterPath,
    backdropImage = backdropPath,
    releaseDate = releaseDate?.let { DateFormat("yyyy-MM-dd").tryParse(it) },
    voteAverage = voteAverage
)
