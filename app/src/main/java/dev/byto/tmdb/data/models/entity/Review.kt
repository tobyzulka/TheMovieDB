package dev.byto.tmdb.data.models.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Review(
    @Json(name = "author") val author: String,
    @Json(name = "author_details") val authorDetails: AuthorDetail,
    @Json(name = "content") val content: String,
    @Json(name = "created_at") val createdAt: String,
    @Json(name = "id") val id: String,
    @Json(name = "updated_at") val updateAt: String,
    @Json(name = "url") val url: String
) {
    @JsonClass(generateAdapter = true)
    data class AuthorDetail(
        @Json(name = "name") val name: String?,
        @Json(name = "username") val username: String,
        @Json(name = "avatar_path") val avatarPath: String?,
        @Json(name = "rating") val rating: Int?
    )
}