package dev.byto.tmdb.data.models.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import dev.byto.tmdb.data.models.entity.Genre

@JsonClass(generateAdapter = true)
data class GenreResponse(
    @Json(name="genres") val genres: List<Genre>
)
