package dev.byto.tmdb.di.module

import dev.byto.tmdb.data.repository.MovieRepository
import dev.byto.tmdb.data.remote.ApiService
import dev.byto.tmdb.data.repository.DiscoverRepository
import dev.byto.tmdb.data.repository.ReviewRepository
import dev.byto.tmdb.data.repository.SearchRepository
import dev.byto.tmdb.data.repository.TrendingRepository
import dev.byto.tmdb.di.component.createTmdbWebService
import org.koin.dsl.module

val dataModule = module {

    single { createTmdbWebService<ApiService>() }

    // Dashboard
    factory { MovieRepository(get()) }
    factory { TrendingRepository(get()) }
    // Detail
    factory { MovieRepository(get()) }
    factory { ReviewRepository(get()) }
    // Search
    factory { SearchRepository(get()) }
    factory { DiscoverRepository(get()) }
}