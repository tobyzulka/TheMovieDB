package dev.byto.tmdb.di

import dev.byto.tmdb.di.module.dataModule
import dev.byto.tmdb.di.module.viewModelsModule

val appComponent = listOf(
    dataModule,
    viewModelsModule
)