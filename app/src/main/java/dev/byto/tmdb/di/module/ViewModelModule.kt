package dev.byto.tmdb.di.module

import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.ui.collection.genres.GenresViewModel
import dev.byto.tmdb.ui.collection.CollectionsViewModel
import dev.byto.tmdb.ui.dashboard.movies.MoviesViewModel
import dev.byto.tmdb.ui.detail.DetailsViewModel
import dev.byto.tmdb.ui.detail.images.ImagesViewModel
import dev.byto.tmdb.ui.detail.reviews.ReviewViewModel
import dev.byto.tmdb.ui.search.discover.DiscoverViewModel
import dev.byto.tmdb.ui.search.filters.DiscoverFiltersViewModel
import dev.byto.tmdb.ui.search.movie.SearchMovieViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    // Dashboard
    viewModel { MoviesViewModel(get(), get()) }

    // Collection
    viewModel { GenresViewModel(get()) }
    viewModel { (movieCollectionType: MovieCollectionType) ->
        CollectionsViewModel(
            get(),
            movieCollectionType
        )
    }

    // Detail
    viewModel { DetailsViewModel(get()) }
    viewModel { ReviewViewModel(get()) }
    viewModel { ImagesViewModel(get()) }

    // Search
    viewModel { SearchMovieViewModel(get()) }
    viewModel { DiscoverFiltersViewModel(get()) }
    viewModel { DiscoverViewModel(get()) }
}