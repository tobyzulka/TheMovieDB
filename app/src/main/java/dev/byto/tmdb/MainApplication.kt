package dev.byto.tmdb

import android.app.Application
import android.content.Context
import android.os.Build
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate
import androidx.multidex.MultiDex
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dev.byto.tmdb.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import java.util.*

class MainApplication : Application() {

    private var localizationDelegate = LocalizationApplicationDelegate()

    override fun onCreate() {
        super.onCreate()

        Logger.addLogAdapter(AndroidLogAdapter())
        Timber.plant(object : Timber.DebugTree() {
            override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                Logger.log(priority, tag, message, t)
            }
        })

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@MainApplication)
            modules(appComponent)
        }
    }

    override fun attachBaseContext(base: Context) {
        attachLocaleToBaseContext(base)

        super.attachBaseContext(base)

        MultiDex.install(this)
    }

    private fun attachLocaleToBaseContext(base: Context) {
        val locale: Locale =
            base.resources.configuration.locales.get(0) ?: Locale.ENGLISH

        localizationDelegate.setDefaultLanguage(base, locale)
    }
}