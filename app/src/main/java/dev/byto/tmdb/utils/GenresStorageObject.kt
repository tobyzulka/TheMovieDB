package dev.byto.tmdb.utils

import android.util.SparseArray

object GenresStorageObject {
    val movieGenres: SparseArray<String> = SparseArray()
}
