package dev.byto.tmdb.utils

import androidx.recyclerview.widget.DiffUtil
import dev.byto.tmdb.data.models.BaseDiscoverDomainModel
import dev.byto.tmdb.data.models.entity.Genre
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.data.models.entity.Review

fun getMovieDiffUtils() = object : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(
        oldItem: Movie,
        newItem: Movie
    ): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: Movie,
        newItem: Movie
    ): Boolean = oldItem.id == newItem.id
}

fun getDiscoverDiffUtils() = object : DiffUtil.ItemCallback<BaseDiscoverDomainModel>() {
    override fun areItemsTheSame(
        oldItem: BaseDiscoverDomainModel,
        newItem: BaseDiscoverDomainModel
    ): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: BaseDiscoverDomainModel,
        newItem: BaseDiscoverDomainModel
    ): Boolean = oldItem == newItem
}
fun getGenreDiffUtils() = object : DiffUtil.ItemCallback<Genre>() {
    override fun areItemsTheSame(
        oldItem: Genre,
        newItem: Genre
    ): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: Genre,
        newItem: Genre
    ): Boolean = oldItem == newItem
}