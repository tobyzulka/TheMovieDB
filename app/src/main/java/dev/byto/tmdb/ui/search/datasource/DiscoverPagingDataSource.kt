package dev.byto.tmdb.ui.search.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.orhanobut.logger.Logger
import dev.byto.tmdb.base.enums.DiscoverType
import dev.byto.tmdb.data.models.BaseDiscoverDomainModel
import dev.byto.tmdb.data.models.DiscoverFiltersModel
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.models.toDiscoverDomainModel
import dev.byto.tmdb.data.repository.DiscoverRepository

class DiscoverPagingDataSource(
    private val repository: DiscoverRepository,
    private val language: String?,
    private val filtersModel: DiscoverFiltersModel
) : PagingSource<Int, BaseDiscoverDomainModel>() {

    private var totalPages: Int? = null

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, BaseDiscoverDomainModel> {
        // Start refresh at page 1 if undefined.
        val nextPageNumber = params.key ?: 1

        if (totalPages != null && nextPageNumber > totalPages!!) {
            return LoadResult.Error(Exception("Max page. nextPage: $nextPageNumber, maxPage: $totalPages"))
        }

        return if (filtersModel.discoverType == DiscoverType.MOVIES) {
            loadMovies(nextPageNumber)
        } else {
            loadMovies(nextPageNumber)
        }
    }

    private suspend fun loadMovies(
        nextPageNumber: Int
    ): LoadResult<Int, BaseDiscoverDomainModel> {
        when (val response = repository.getDiscoverMovies(language, nextPageNumber, filtersModel)) {
            is ResultWrapper.GenericError -> {
                Logger.e("Generic Error")

                return LoadResult.Error(
                    Exception(
                        "Generic Error  ${response.error?.statusMessage} ${response.error?.statusCode}"
                    )
                )
            }

            is ResultWrapper.NetworkError -> {
                Logger.e("Network Error")
                return LoadResult.Error(Exception("Network Error}"))
            }

            is ResultWrapper.Success -> {
                totalPages = response.value.totalPages

                return LoadResult.Page(
                    data = response.value.results.map { it.toDiscoverDomainModel() },
                    prevKey = null, // Only paging forward.
                    nextKey = nextPageNumber + 1
                )
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, BaseDiscoverDomainModel>): Int? = null
}
