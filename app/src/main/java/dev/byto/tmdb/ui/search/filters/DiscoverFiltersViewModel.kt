package dev.byto.tmdb.ui.search.filters

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.models.DiscoverFiltersModel
import dev.byto.tmdb.data.models.network.GenreResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.MovieRepository
import kotlinx.coroutines.launch

class DiscoverFiltersViewModel(
    private val movieRepository: MovieRepository
) : ViewModel() {

    /**
     * Filters model
     */
    var filtersModel: DiscoverFiltersModel? = null

    /**
     * Selected genres
     */
    val selectedMovieGenres = mutableSetOf<Int>()

    /**
     * liveData
     */
    val movieGenresLiveData = MutableLiveData<GenreResponse?>()

    fun getMovieGenres() {
        viewModelScope.launch {
            when (val response = movieRepository.getMovieGenres(AppConfig.REGION)) {
                is ResultWrapper.NetworkError -> {
                }

                is ResultWrapper.GenericError -> {
                }

                is ResultWrapper.Success -> {
                    movieGenresLiveData.postValue(response.value)
                }
            }
        }
    }
}