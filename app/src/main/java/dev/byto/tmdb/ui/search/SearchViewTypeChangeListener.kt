package dev.byto.tmdb.ui.search

internal interface SearchViewTypeChangeListener {
    fun changeViewType(isGrid: Boolean)
}
