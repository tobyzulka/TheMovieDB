package dev.byto.tmdb.ui.search.movie

import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemMediumPosterCardBinding

internal class SearchMovieSmallVH(val viewBinding: ItemMediumPosterCardBinding) :
    RecyclerView.ViewHolder(viewBinding.root) {

    fun bindTo(item: Movie?) {
        item?.let {
            viewBinding.collectionImage.displayImageWithCenterCrop(
                UrlConstants.TMDB_POSTER_SIZE_185 + it.posterPath
            )
            viewBinding.title.text = it.title
        }
    }
}
