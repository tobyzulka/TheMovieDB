package dev.byto.tmdb.ui.dashboard.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.databinding.ItemLanguageBinding
import java.util.*
import dev.byto.tmdb.ui.dashboard.adapters.LanguageAdapter.LanguageViewHolder


class LanguageAdapter(private val listener: (locale: Locale) -> Unit) :
    RecyclerView.Adapter<LanguageViewHolder>() {

    private val languages: List<Locale> = AppConfig.AVAILABLE_LANGUAGES

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemLanguageBinding.inflate(layoutInflater, parent, false)

        return LanguageViewHolder(viewBinding)
    }

    override fun getItemCount(): Int = AppConfig.AVAILABLE_LANGUAGES.size

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        holder.bindTo(languages[position])

        holder.itemView.setSafeOnClickListener {
            listener(languages[position])
            notifyDataSetChanged()
        }
    }

    class LanguageViewHolder(val viewBinding: ItemLanguageBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(locale: Locale) {
            viewBinding.langName.text = locale.displayName.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(
                    AppConfig.APP_LOCALE
                ) else it.toString()
            }
            if (AppConfig.APP_LOCALE.toLanguageTag() == locale.toLanguageTag()) {
                viewBinding.languageLayout.setCardBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorAccent
                    )
                )
            } else {
                viewBinding.languageLayout.setCardBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.alabaster
                    )
                )
            }
        }
    }
}
