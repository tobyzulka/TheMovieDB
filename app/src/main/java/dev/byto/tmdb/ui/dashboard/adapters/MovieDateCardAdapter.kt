package dev.byto.tmdb.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.base.extensions.toDate
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemSmallPosterDateCardBinding
import dev.byto.tmdb.utils.getMovieDiffUtils
import dev.byto.tmdb.ui.dashboard.adapters.MovieDateCardAdapter.MovieViewHolder
import korlibs.time.KlockLocale

class MovieDateCardAdapter(private val clickAction: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, MovieViewHolder>(getMovieDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSmallPosterDateCardBinding.inflate(layoutInflater, parent, false)

        return MovieViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.bindTo(movie)

            holder.viewBinding.collectionCard.setSafeOnClickListener {
                clickAction(movie.id)
            }
        }
    }

    class MovieViewHolder(val viewBinding: ItemSmallPosterDateCardBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(movie: Movie) {
            val releaseDate = movie.releaseDate?.toDate()
            viewBinding.placeholderText.text = movie.title

            val date = "${releaseDate?.dayOfMonth ?: ""} ${releaseDate?.month?.localShortName(KlockLocale.english) ?: ""}"
            viewBinding.mReleaseDate.text = date


            viewBinding.collectionImage.displayImageWithCenterCrop(
                UrlConstants.TMDB_POSTER_SIZE_185 + movie.posterPath
            )
        }
    }
}
