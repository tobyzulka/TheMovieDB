package dev.byto.tmdb.ui.collection.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.data.models.entity.Genre
import dev.byto.tmdb.databinding.ItemGenreInlineBinding
import dev.byto.tmdb.ui.collection.adapters.GenresInlineAdapter.GenreViewHolder
import dev.byto.tmdb.utils.getGenreDiffUtils

class GenresInlineAdapter(private val clickAction: (name: String) -> Unit) :
    PagingDataAdapter<Genre, GenreViewHolder>(
        getGenreDiffUtils()
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenreViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemGenreInlineBinding.inflate(
            layoutInflater,
            parent,
            false
        )

        return GenreViewHolder(viewBinding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        getItem(position)?.let { genre ->
            holder.bindTo(genre)

            holder.itemView.setSafeOnClickListener {
                clickAction(genre.name)
            }
        }
    }

    class GenreViewHolder(
        private val viewBinding: ItemGenreInlineBinding
    ) : RecyclerView.ViewHolder(viewBinding.root) {
        fun bindTo(genre: Genre) {
            viewBinding.genreName.text = genre.name
        }
    }
}
