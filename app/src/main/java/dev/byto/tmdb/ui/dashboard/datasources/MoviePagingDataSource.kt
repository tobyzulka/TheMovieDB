package dev.byto.tmdb.ui.dashboard.datasources

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.orhanobut.logger.Logger
import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.base.enums.MovieCollectionType.*
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.data.models.network.MovieResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.MovieRepository

class MoviePagingDataSource(
    private val repository: MovieRepository,
    private val collectionType: MovieCollectionType
) : PagingSource<Int, Movie>() {

    private var totalPages: Int? = null

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? = null

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Movie> {
        // Start refresh at page 1 if undefined.
        val nextPageNumber = params.key ?: 1

        if (totalPages != null && nextPageNumber > totalPages!!) {
            return LoadResult.Error(Exception("Max page. nextPage: $nextPageNumber, maxPage: $totalPages"))
        }

        return when (val response = getMoviesCollectionsByType(nextPageNumber)) {
            is ResultWrapper.GenericError -> {
                Logger.e("Generic Error")

                LoadResult.Error(
                    Exception(
                        "Generic Error  ${response.error?.statusMessage} ${response.error?.statusCode}"
                    )
                )
            }

            is ResultWrapper.NetworkError -> {
                Logger.e("Network Error")
                LoadResult.Error(Exception("Network Error}"))
            }

            is ResultWrapper.Success -> {
                totalPages = response.value.totalPages

                LoadResult.Page(
                    data = response.value.results,
                    prevKey = null, // Only paging forward.
                    nextKey = nextPageNumber + 1
                )
            }
        }
    }

    private suspend fun getMoviesCollectionsByType(nextPage: Int): ResultWrapper<MovieResponse> {
        return when (collectionType) {
            POPULAR -> {
                repository.getPopularMovies(AppConfig.REGION, nextPage, null)
            }

            TOP_RATED -> {
                repository.getTopRatedMovies(AppConfig.REGION, nextPage, null)
            }

            UPCOMING -> {
                repository.getUpcomingMovies(AppConfig.REGION, nextPage, null)
            }

            NOW_PLAYING -> {
                repository.getNowPlayingMovies(AppConfig.REGION, nextPage, null)
            }
        }
    }
}
