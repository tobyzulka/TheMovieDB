package dev.byto.tmdb.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemSmallPosterCardBinding
import dev.byto.tmdb.utils.getMovieDiffUtils
import dev.byto.tmdb.ui.dashboard.adapters.MovieCollectionAdapter.MovieViewHolder

class MovieCollectionAdapter(private val clickAction: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, MovieViewHolder> (getMovieDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            ItemSmallPosterCardBinding.inflate(layoutInflater, parent, false)

        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.bindTo(movie)

            holder.viewBinding.collectionCard.setSafeOnClickListener {
                clickAction(movie.id)
            }
        }
    }

    class MovieViewHolder(val viewBinding: ItemSmallPosterCardBinding) : RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(movie: Movie) {
            viewBinding.placeholderText.text = movie.title

            viewBinding.collectionImage.displayImageWithCenterCrop(
                UrlConstants.TMDB_POSTER_SIZE_185 + movie.posterPath
            )
        }
    }
}
