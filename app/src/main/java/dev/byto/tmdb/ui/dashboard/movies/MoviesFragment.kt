package dev.byto.tmdb.ui.dashboard.movies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.util.isEmpty
import androidx.core.view.updatePadding
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.enums.MovieCollectionType.*
import dev.byto.tmdb.base.extensions.doOnApplyWindowInsets
import dev.byto.tmdb.base.extensions.px
import dev.byto.tmdb.base.extensions.setAdapterWithFixedSize
import dev.byto.tmdb.base.extensions.setDarkNavigationBarColor
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.base.extensions.setShowSideItems
import dev.byto.tmdb.data.models.network.GenreResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.databinding.FragmentMoviesBinding
import dev.byto.tmdb.ui.dashboard.adapters.GenreAdapter
import dev.byto.tmdb.ui.dashboard.adapters.MovieCardAdapter
import dev.byto.tmdb.ui.dashboard.adapters.MovieCollectionAdapter
import dev.byto.tmdb.ui.dashboard.adapters.MovieDateCardAdapter
import dev.byto.tmdb.ui.dashboard.adapters.MovieTrendAdapter
import dev.byto.tmdb.utils.GenresStorageObject
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesFragment : Fragment(R.layout.fragment_movies) {

    private val viewBinding: FragmentMoviesBinding by viewBinding()

    private val viewModel: MoviesViewModel by viewModel()

    /**
     * Adapters
     */
    private val nowPlayingMoviesAdapter = MovieCollectionAdapter(::actionClickMovie)
    private val upcomingMoviesAdapter = MovieDateCardAdapter(::actionClickMovie)
    private val popularMoviesAdapter = MovieCardAdapter(::actionClickMovie)
    private val trendingMovieAdapter = MovieTrendAdapter(::actionClickMovie)

    private val genresAdapter = GenreAdapter(::actionClickGenre)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.setDarkNavigationBarColor()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateMargins()

        initAdapters()
        setObservers()
        setClickListeners()
    }

    private fun updateMargins() {
        viewBinding.root.doOnApplyWindowInsets { _, insets, _ ->
            viewBinding.scrollViewLinearLayout.updatePadding(bottom = insets.systemWindowInsetBottom + 56.px())

            insets
        }
    }

    private fun initAdapters() {
        lifecycleScope.launchWhenResumed {
            viewBinding.vpPopularMovies.adapter = popularMoviesAdapter
            viewBinding.vpPopularMovies.setShowSideItems(16.px(), 16.px())

            viewBinding.rvNowPlayingMovies.setAdapterWithFixedSize(
                ScaleInAnimationAdapter(nowPlayingMoviesAdapter),true
            )

            viewBinding.rvUpcomingMovies.setAdapterWithFixedSize(
                upcomingMoviesAdapter,
                true
            )

            viewBinding.rvMovieGenres.setAdapterWithFixedSize(genresAdapter, true)

            viewBinding.vpTrendMovies.adapter = trendingMovieAdapter
        }
    }

    private fun setClickListeners() {
        viewBinding.apply {
            btnSearch.setOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieCollections(POPULAR))
            }

            btnPopularMovies.setOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieCollections(POPULAR))
            }

            btnUpcomingMovies.setOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieCollections(UPCOMING))
            }

            btnNowPlayingMovies.setOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieCollections(NOW_PLAYING))
            }

            btnTrendingMovies.setOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieCollections(POPULAR))
            }

            btnMovieGenres.setSafeOnClickListener {
                findNavController().navigate(MoviesFragmentDirections.actionToMovieGenres())
            }
        }
    }

    private fun setObservers() {
        viewModel.run {
            lifecycleScope.launchWhenResumed {
                nowPlayingMoviesFlow.collectLatest { pagingData ->
                    nowPlayingMoviesAdapter.submitData(pagingData)
                }
            }

            lifecycleScope.launchWhenResumed {
                upcomingMoviesFlow.collectLatest { pagingData ->
                    upcomingMoviesAdapter.submitData(pagingData)
                }
            }

            lifecycleScope.launchWhenResumed {
                popularMoviesFlow.collectLatest { pagingData ->
                    popularMoviesAdapter.submitData(pagingData)
                }
            }

            lifecycleScope.launchWhenResumed {
                trendingMoviesFlow.collectLatest { pagingData ->
                    trendingMovieAdapter.submitData(pagingData)
                }
            }

        }

        viewModel.run {
            genresLiveData.observe(
                viewLifecycleOwner
            ) {
                setMovieGenres(it)
            }
        }
    }

    /**
     * Actions
     */
    private fun actionClickMovie(id: Int) {
        findNavController().navigate(MoviesFragmentDirections.actionToMovieDetails(id))
    }
    private fun actionClickGenre(id: Int) {
        findNavController().navigate(MoviesFragmentDirections.actionToMovieDetails(id))
    }

    /**
     * Setters
     */
    private fun setMovieGenres(result: ResultWrapper<GenreResponse>?) {
        when (result) {
            is ResultWrapper.Success -> {
                if (GenresStorageObject.movieGenres.isEmpty()) {
                    result.value.genres.map { genre ->
                        GenresStorageObject.movieGenres.put(
                            genre.id,
                            genre.name
                        )
                    }
                }

                genresAdapter.addItems(result.value.genres)
            }

            else -> {}
        }
    }
}