package dev.byto.tmdb.ui.dashboard.settings.clear_dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.R as AppRes
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.databinding.DialogClearCacheBinding
import dev.byto.tmdb.utils.FontSpan

class ClearCacheDialog : DialogFragment(R.layout.dialog_clear_cache) {

    private val viewBinding: DialogClearCacheBinding by viewBinding()

    private val args: ClearCacheDialogArgs by navArgs()

    override fun onStart() {
        super.onStart()

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.cacheSize.text = getString(R.string.navigation_cache_size_with_format,
            args.cacheSize
        )
        setSpannableCacheSizeText()

        viewBinding.btnCancel.setOnClickListener {
            dismiss()
        }

        viewBinding.btnClearNow.setSafeOnClickListener {
            context?.cacheDir?.deleteRecursively()
            (activity as? ClearDialogListener)?.onDialogDismiss()
            dismiss()
        }
    }

    private fun setSpannableCacheSizeText() {
        SpannableStringBuilder(viewBinding.cacheSize.text).apply {
            setSpan(
                FontSpan(
                    "inter_semibold",
                    ResourcesCompat.getFont(
                        viewBinding.root.context,
                        AppRes.font.inter_semibold
                    )
                ),
                viewBinding.cacheSize.text.length - args.cacheSize.length,
                viewBinding.cacheSize.text.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            viewBinding.cacheSize.text = this
        }
    }
}
