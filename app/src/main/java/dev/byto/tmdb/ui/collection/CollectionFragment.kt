package dev.byto.tmdb.ui.collection

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dev.byto.tmdb.R
import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.base.extensions.px
import dev.byto.tmdb.databinding.FragmentMovieCollectionBinding
import dev.byto.tmdb.ui.collection.adapters.PagingMovieCollectionAdapter
import dev.byto.tmdb.utils.SpacingDecoration
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import by.kirich1409.viewbindingdelegate.viewBinding
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CollectionFragment(
    private val collectionType: MovieCollectionType?,
    private val name: String?
) : Fragment(R.layout.fragment_movie_collection) {

    private val viewBinding: FragmentMovieCollectionBinding by viewBinding()

    private val viewModel: CollectionsViewModel by viewModel { parametersOf(collectionType) }

    private val viewModelGenre: CollectionsViewModel by viewModel()

    private val adapter: PagingMovieCollectionAdapter = PagingMovieCollectionAdapter(::openMovieDetails)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()
        setObservers()
    }

    private fun configureRecyclerView() {
        viewBinding.gridItems.addItemDecoration(SpacingDecoration(16.px(), 16.px(), true))
        viewBinding.gridItems.adapter = ScaleInAnimationAdapter(adapter)
    }

    private fun setObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.moviesFlow.collectLatest { pagingData ->
                adapter.submitData(pagingData)
            }
        }
    }

    /**
     * Actions
     */
    private fun openMovieDetails(id: Int) {
        findNavController().navigate(TabsMovieCollectionsFragmentDirections.actionToMovieDetails(id))
    }
}