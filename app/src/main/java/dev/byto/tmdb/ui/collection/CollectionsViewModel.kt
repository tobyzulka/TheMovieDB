package dev.byto.tmdb.ui.collection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.data.repository.MovieRepository
import dev.byto.tmdb.ui.collection.datasources.MovieCollectionsPagingDataSource

class CollectionsViewModel(
    movieRepository: MovieRepository,
    movieCollectionType: MovieCollectionType
) : ViewModel() {

    /**
     * Movies flow
     */
    private var datasource: MovieCollectionsPagingDataSource? = null

    var moviesFlow = Pager(PagingConfig(pageSize = 20)) {
        MovieCollectionsPagingDataSource(movieRepository, movieCollectionType).also {
            datasource = it
        }
    }.flow.cachedIn(viewModelScope)
}