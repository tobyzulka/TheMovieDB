package dev.byto.tmdb.ui.collection.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemMediumPosterCardBinding
import dev.byto.tmdb.utils.getMovieDiffUtils

class PagingMovieCollectionAdapter(private val actionClick: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, PagingMovieCollectionAdapter.MoviePagedViewHolder>(getMovieDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviePagedViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemMediumPosterCardBinding.inflate(layoutInflater, parent, false)

        return MoviePagedViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: MoviePagedViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.bindTo(movie)

            holder.viewBinding.collectionCard.setSafeOnClickListener {
                actionClick(movie.id)
            }
        }
    }

    class MoviePagedViewHolder(val viewBinding: ItemMediumPosterCardBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bindTo(movie: Movie) {
            viewBinding.title.text = movie.title

            viewBinding.collectionImage.displayImageWithCenterCrop(
                UrlConstants.TMDB_POSTER_SIZE_185 + movie.posterPath
            )
        }
    }
}
