package dev.byto.tmdb.ui

import android.os.Bundle
import androidx.core.view.updatePadding
import androidx.navigation.NavController
import androidx.navigation.dynamicfeatures.fragment.DynamicNavHostFragment
import androidx.navigation.ui.setupWithNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.doOnApplyWindowInsets
import dev.byto.tmdb.base.extensions.setVisibilityOption
import dev.byto.tmdb.base.extensions.updateMargin
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.databinding.ActivityMainBinding

class MainActivity : LocalizationActivity() {

    private val viewBinding: ActivityMainBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        updateMargins()
        updateBottomNavPaddings()

        AppConfig.REGION = getCurrentLanguage().toString()

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
    }

    private fun updateMargins() {
        viewBinding.root.doOnApplyWindowInsets { _, insets, _ ->
            viewBinding.mainFragmentContainer.updateMargin(top = insets.systemWindowInsetTop)

            insets
        }
    }

    private fun updateBottomNavPaddings() {
        viewBinding.navView.doOnApplyWindowInsets { view, insets, _ ->
            view.updatePadding(bottom = 0)
            insets
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        setupBottomNavigationBar()
    }

    private fun setupBottomNavigationBar() {
        val navHost = supportFragmentManager.findFragmentById(R.id.main_fragment_container) as DynamicNavHostFragment

        val controller = navHost.navController

        viewBinding.navView.setupWithNavController(navHost.navController)

        val listener = NavController.OnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                in getNavigationIdsForBottomNavVisible() -> {
                    viewBinding.navView.setVisibilityOption(true)
                }

                else -> {
                    viewBinding.navView.setVisibilityOption(false)
                }
            }
        }

        controller.removeOnDestinationChangedListener(listener)
        controller.addOnDestinationChangedListener(listener)
    }

    private fun getNavigationIdsForBottomNavVisible() = listOf(
        R.id.nav_discover_id,
        R.id.nav_search_id,
        R.id.nav_setting_id
    )
}