package dev.byto.tmdb.ui.search.discover

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.models.DiscoverFiltersModel
import dev.byto.tmdb.data.repository.DiscoverRepository
import dev.byto.tmdb.ui.search.datasource.DiscoverPagingDataSource

class DiscoverViewModel(private val repository: DiscoverRepository) : ViewModel() {

    var filtersModel: DiscoverFiltersModel = DiscoverFiltersModel()

    private var discoverDataSource: DiscoverPagingDataSource? = null

    private var language: String? = AppConfig.REGION

    val discoverFlow = Pager(PagingConfig(pageSize = 20)) {
        DiscoverPagingDataSource(
            repository = repository,
            language = language,
            filtersModel = filtersModel
        ).also {
            discoverDataSource = it
        }
    }.flow.cachedIn(viewModelScope)
}
