package dev.byto.tmdb.ui.search.movie

import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setIndicatorColor
import dev.byto.tmdb.base.extensions.toDate
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemCardWithDetailsBinding
import dev.byto.tmdb.utils.GenresStorageObject

internal class SearchMovieDetailedVH(val viewBinding: ItemCardWithDetailsBinding) : RecyclerView.ViewHolder(viewBinding.root) {

    fun bindTo(item: Movie?) {
        item?.let { it ->
            viewBinding.cardImage.displayImageWithCenterCrop(UrlConstants.TMDB_BACKDROP_SIZE_1280 + it.backdropPath)
            viewBinding.cardTitle.text = it.title

            if (!it.releaseDate.isNullOrEmpty()) {
                viewBinding.cardReleaseDate.text = it.releaseDate.toDate().yearInt.toString()
            }

            viewBinding.cardVoteAverage.text = it.voteAverage.toString()

            viewBinding.cardGenres.text = it.genreIds.map { GenresStorageObject.movieGenres.get(it) }
                .joinToString(" - ") { s ->
                    (s ?: "").replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(
                        AppConfig.APP_LOCALE
                    ) else it.toString()
                } }

            viewBinding.voteAverageIndicator.setIndicatorColor(item.voteAverage)
        }
    }
}
