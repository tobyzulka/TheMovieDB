package dev.byto.tmdb.ui.search.filters

import android.animation.LayoutTransition
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.orhanobut.logger.Logger
import dev.byto.tmdb.base.enums.DiscoverType
import dev.byto.tmdb.base.enums.ReleaseDateType
import dev.byto.tmdb.base.enums.SortFeature
import dev.byto.tmdb.base.enums.SortType
import dev.byto.tmdb.base.enums.isMoviesType
import dev.byto.tmdb.base.extensions.setVisibilityOption
import dev.byto.tmdb.R
import dev.byto.tmdb.base.enums.isMoviesTypeWithAction
import dev.byto.tmdb.R as AppRes
import dev.byto.tmdb.data.models.DiscoverFiltersModel
import dev.byto.tmdb.data.models.entity.Genre
import dev.byto.tmdb.databinding.DialogDiscoverFiltersBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.math.roundToInt

class DiscoverFiltersBottomSheetFragment : BottomSheetDialogFragment() {

    private val viewBinding: DialogDiscoverFiltersBinding by viewBinding()

    private val viewModel: DiscoverFiltersViewModel by viewModel()

    /**
     * Companion object
     */
    companion object {
        const val KEY_FILTERS = "key_filters"
        const val KEY_DISCOVER_FILTERS_REQUEST = "key_discover_filters_request"
        const val KEY_FILTERS_MODEL = "key_filters_model"

        const val RELEASE_DATE_MIN_YEAR = 1945
        const val RELEASE_DATE_RANGE_OFFSET = 5
        const val RELEASE_DATE_SINGLE_OFFSET = 10

        private var releaseDateType = ReleaseDateType.ANY

        fun newInstance(filtersModel: DiscoverFiltersModel) = DiscoverFiltersBottomSheetFragment().apply {
            arguments = Bundle().apply {
                putSerializable(KEY_FILTERS, filtersModel)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(DialogFragment.STYLE_NORMAL, AppRes.style.Widget_AppTheme_BottomSheet)

        viewModel.filtersModel =
            (arguments?.getSerializable(KEY_FILTERS) as? DiscoverFiltersModel) ?: DiscoverFiltersModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_discover_filters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchGenres()
        setClickListeners()
        setListeners()

        initReleaseDateRange()
        initReleaseDateSingle()
        initRatingBar()

        setAnimateReleaseDateBlock()
        setObservers()
        setupFilters()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        saveFiltersToModel()

        setFragmentResult(
            KEY_DISCOVER_FILTERS_REQUEST,
            Bundle().apply {
                putSerializable(KEY_FILTERS_MODEL, viewModel.filtersModel)
            }
        )
    }

    private fun setObservers() {
        viewModel.movieGenresLiveData.observe(
            viewLifecycleOwner
        ) {
            getDiscoverType().isMoviesTypeWithAction {
                if (it != null) {
                    addGenresBlock(it.genres)
                }
            }
        }
    }

    private fun fetchGenres() {
        lifecycleScope.launchWhenResumed {
            viewModel.getMovieGenres()
        }
    }

    private fun setClickListeners() {
        viewBinding.apply {
            layoutReleaseDate.setOnClickListener {
                displayReleaseDateChipsMenu()
            }

            chipDateAnyDates.setOnClickListener {
                releaseDateType = ReleaseDateType.ANY
                changeReleaseDateType()
            }

            chipDateBetweenDates.setOnClickListener {
                releaseDateType = ReleaseDateType.BETWEEN_DATES
                changeReleaseDateType()
            }

            chipDateOneYear.setOnClickListener {
                releaseDateType = ReleaseDateType.ONE_YEAR
                changeReleaseDateType()
            }

            genresBlock.setOnClickListener {
                displayGenresBlock()
            }

            btnClose.setOnClickListener {
                findNavController().popBackStack()
                saveFiltersToModel()
            }

            btnResetFilters.setOnClickListener{
                viewModel.filtersModel?.reset()
            }
        }
    }

    private fun setListeners() {
        setReleaseDateRangeListener()
        setReleaseDateSingleListener()
        setRatingBarListener()
    }

    private fun changeFiltersType() {
        if (getDiscoverType().isMoviesType()) {
            if (viewModel.movieGenresLiveData.value?.genres.isNullOrEmpty()) {
                viewModel.getMovieGenres()
            } else {
                addGenresBlock(viewModel.movieGenresLiveData.value!!.genres)
            }
        }

        setupIncludeAdultUI()
    }

    private fun saveFiltersToModel() {
        viewModel.filtersModel?.apply {
            discoverType = getDiscoverType()

            sortType = getSortType()
            sortFeature = getSortFeature()

            minYear = getMinReleaseDate()
            maxYear = getMaxReleaseDate()

            voteAverageMin = viewBinding.ratingBar.leftSeekBar.progress
            voteAverageMax = viewBinding.ratingBar.rightSeekBar.progress

            includeAdult = viewBinding.sIncludeAdult.isChecked

            movieGenresIds = viewModel.selectedMovieGenres.toList()
        }
    }

    /**
     * Release Date block
     */
    private fun initReleaseDateRange() {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)

        viewBinding.releaseDateRange.apply {
            setRange(RELEASE_DATE_MIN_YEAR.toFloat(), (currentYear + RELEASE_DATE_RANGE_OFFSET).toFloat(), 1f)
            setProgress(minProgress, maxProgress)
        }
    }

    private fun setReleaseDateRangeListener() {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val maxProgress = viewBinding.releaseDateRange.maxProgress.roundToInt()

        viewBinding.releaseDateRange.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                val leftInt = leftValue.roundToInt()
                val rightInt = rightValue.roundToInt()

                val maxYear =
                    if (maxProgress == rightInt) "${currentYear + RELEASE_DATE_RANGE_OFFSET}+" else rightInt
                        .toString()

                val set = "$leftInt - $maxYear"
                viewBinding.descriptionReleaseDate.text = set
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }
        })
    }

    private fun initReleaseDateSingle() {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)

        viewBinding.releaseDateSingle.apply {
            setRange(RELEASE_DATE_MIN_YEAR.toFloat(), (currentYear + RELEASE_DATE_SINGLE_OFFSET).toFloat(), 1f)

            setProgress(currentYear.toFloat())
        }
    }

    private fun setReleaseDateSingleListener() {
        viewBinding.releaseDateSingle.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                viewBinding.descriptionReleaseDate.text = "${leftValue.roundToInt()}"
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }
        })
    }

    private fun changeReleaseDateType() {
        when (releaseDateType) {
            ReleaseDateType.ANY -> {
                displayAnyReleaseDate()
            }

            ReleaseDateType.BETWEEN_DATES -> {
                displayReleaseDateRange()
            }

            ReleaseDateType.ONE_YEAR -> {
                displayReleaseDateSingleBar()
            }
        }
    }

    private fun displayReleaseDateChipsMenu() {
        viewBinding.apply {
            if (releaseDateChipGroup.visibility == View.VISIBLE) {
                changeReleaseDateType()
            } else {
                releaseDateRange.setVisibilityOption(false)
                releaseDateSingle.setVisibilityOption(false)
                releaseDateChipGroup.setVisibilityOption(true)
            }
        }
    }

    private fun displayAnyReleaseDate() {
        viewBinding.apply {
            releaseDateChipGroup.setVisibilityOption(false)

            descriptionReleaseDate.text = getString(R.string.filters_dialog_any)
        }
    }

    private fun displayReleaseDateRange() {
        viewBinding.apply {
            releaseDateChipGroup.setVisibilityOption(false)
            releaseDateRange.setVisibilityOption(true)

            val leftProgress = releaseDateRange.leftSeekBar.progress
            val rightProgress = releaseDateRange.rightSeekBar.progress
            releaseDateRange.setProgress(leftProgress, rightProgress)
        }
    }

    private fun displayReleaseDateSingleBar() {
        viewBinding.apply {
            releaseDateChipGroup.setVisibilityOption(false)
            releaseDateSingle.setVisibilityOption(true)

            val progress = releaseDateSingle.leftSeekBar.progress
            releaseDateSingle.setProgress(progress)
        }
    }

    private fun setAnimateReleaseDateBlock() {
        val transition = LayoutTransition()
        transition.setAnimateParentHierarchy(false)
        viewBinding.releaseDateChipGroup.layoutTransition = null
        viewBinding.layoutReleaseDate.layoutTransition = transition
    }

    /**
     * Raring block
     */
    private fun initRatingBar() {
        viewBinding.ratingBar.apply {
            isStepsAutoBonding = true
            steps = 1
            setRange(0f, 100f, 10f)
        }
    }

    private fun setRatingBarListener() {
        viewBinding.ratingBar.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onRangeChanged(view: RangeSeekBar, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                val left = leftValue.roundToInt()
                val right = rightValue.roundToInt()
                if (left == 0 && right == 100) {
                    viewBinding.descriptionRating.text = getString(R.string.filters_dialog_any)
                } else {
                    val set = "${left / 10f} - ${right / 10f}"
                    viewBinding.descriptionRating.text = set
                }
            }

            override fun onStopTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {
            }
        })
    }

    /**
     * Genres block
     */
    private fun addGenresBlock(items: List<Genre>) {
        viewBinding.genresChipGroup.removeAllViews()
        items.forEach { genre ->
            val chip = LayoutInflater.from(context)
                .inflate(R.layout.item_chip_genre, viewBinding.genresChipGroup, false) as Chip
            chip.text = genre.name
            chip.isChecked = checkGenresSelected(genre.id)

            chip.setOnClickListener {
                if (getDiscoverType().isMoviesType()) {
                    clickMovieGenre(chip.isChecked, genre.id)
                }
            }

            viewBinding.genresChipGroup.addView(chip)
        }
    }

    private fun clickMovieGenre(chipChecked: Boolean, genreId: Int) {
        if (chipChecked) {
            viewModel.selectedMovieGenres.add(genreId)
        } else {
            viewModel.selectedMovieGenres.remove(genreId)
        }

        chipGenresSelectedChanged()
    }

    private fun checkGenresSelected(genreId: Int): Boolean {
        return viewModel.selectedMovieGenres.contains(genreId)
    }

    private fun chipGenresSelectedChanged() {
        val countActiveGenres = viewModel.selectedMovieGenres.count()

        if (countActiveGenres == 0) {
            viewBinding.descriptionGenres.text = getString(R.string.filters_dialog_any)
        } else {
            viewBinding.descriptionGenres.text =
                getString(R.string.filters_dialog_selected_genres_format).format(countActiveGenres)
        }
    }

    private fun displayGenresBlock() {
        if (viewBinding.genresChipGroup.visibility == View.VISIBLE) {
            viewBinding.genresChipGroup.setVisibilityOption(false)
        } else {
            viewBinding.genresChipGroup.setVisibilityOption(true)
        }
    }

    /**
     * Methods for changing the interface by filters
     */
    private fun setupFilters() {
        if (viewModel.filtersModel == null) viewModel.filtersModel = DiscoverFiltersModel()

        viewModel.selectedMovieGenres.addAll(viewModel.filtersModel?.movieGenresIds ?: emptyList())

        setupYearsUI()
        setupVoteAverageUI()
        setupSortUI()
        setupIncludeAdultUI()

        chipGenresSelectedChanged()
    }

    private fun setupYearsUI() {
        viewBinding.apply {
            if (viewModel.filtersModel?.minYear == null && viewModel.filtersModel?.maxYear == null) {
                releaseDateType = ReleaseDateType.ANY
                chipDateAnyDates.isChecked = true
            } else if (viewModel.filtersModel?.minYear != null && viewModel.filtersModel?.maxYear == null) {
                releaseDateType = ReleaseDateType.ONE_YEAR
                chipDateOneYear.isChecked = true
                releaseDateSingle.setProgress(
                    viewModel.filtersModel?.minYear?.toFloat() ?: Calendar.getInstance().get(Calendar.YEAR).toFloat()
                )
            } else {
                releaseDateType = ReleaseDateType.BETWEEN_DATES

                chipDateBetweenDates.isChecked = true

                Logger.i("${viewModel.filtersModel?.minYear}, ${viewModel.filtersModel?.maxYear}")

                releaseDateRange.setProgress(
                    viewModel.filtersModel?.minYear?.toFloat() ?: releaseDateRange.minProgress,
                    viewModel.filtersModel?.maxYear?.toFloat() ?: releaseDateRange.maxProgress
                )
            }
        }

        changeReleaseDateType()
    }

    private fun setupVoteAverageUI() {
        viewBinding.apply {
            ratingBar.setProgress(
                viewModel.filtersModel?.voteAverageMin ?: ratingBar.minProgress,
                viewModel.filtersModel?.voteAverageMax ?: ratingBar.maxProgress
            )
        }
    }

    private fun setupSortUI() {
        viewBinding.apply {
            sSortBy.isChecked = viewModel.filtersModel?.sortFeature == SortFeature.ASCENDING

            when (viewModel.filtersModel?.sortType) {
                SortType.BY_POPULARITY -> chipSortPopularity.isChecked = true

                SortType.BY_DATE -> chipSortReleaseDate.isChecked = true

                SortType.BY_VOTE_AVERAGE -> chipSortRating.isChecked = true

                SortType.BY_VOTE_COUNT -> chipSortVoteCount.isChecked = true

                null -> chipSortPopularity.isChecked = true
            }
        }
    }

    private fun setupIncludeAdultUI() {
        viewBinding.sIncludeAdult.isChecked = viewModel.filtersModel?.includeAdult ?: false

        if (getDiscoverType().isMoviesType()) {
            viewBinding.sIncludeAdult.isEnabled = true
            viewBinding.sIncludeAdult.alpha = 1f
        } else {
            viewBinding.sIncludeAdult.isEnabled = false
            viewBinding.sIncludeAdult.alpha = 0.33f
        }
    }

    /**
     * Мethods for getting filters from UI
     */
    private fun getDiscoverType() = DiscoverType.MOVIES

    private fun getSortFeature() = if (viewBinding.sSortBy.isChecked) {
        SortFeature.ASCENDING
    } else {
        SortFeature.DESCENDING
    }

    private fun getMinReleaseDate() = when (releaseDateType) {
        ReleaseDateType.ANY -> {
            null
        }

        ReleaseDateType.ONE_YEAR -> {
            viewBinding.releaseDateSingle.leftSeekBar.progress.roundToInt()
        }

        ReleaseDateType.BETWEEN_DATES -> {
            viewBinding.releaseDateRange.leftSeekBar.progress.roundToInt()
        }
    }

    private fun getMaxReleaseDate() = when (releaseDateType) {
        ReleaseDateType.ANY -> {
            null
        }

        ReleaseDateType.ONE_YEAR -> {
            null
        }

        ReleaseDateType.BETWEEN_DATES -> {
            viewBinding.releaseDateRange.rightSeekBar.progress.roundToInt()
        }
    }

    private fun getSortType(): SortType {
        return when {
            viewBinding.chipSortPopularity.isChecked -> SortType.BY_POPULARITY
            viewBinding.chipSortRating.isChecked -> SortType.BY_VOTE_AVERAGE
            viewBinding.chipSortReleaseDate.isChecked -> SortType.BY_DATE
            viewBinding.chipSortVoteCount.isChecked -> SortType.BY_VOTE_COUNT
            else -> SortType.BY_POPULARITY
        }
    }
}