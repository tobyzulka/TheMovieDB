package dev.byto.tmdb.ui.splash

import android.animation.Animator
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.startActivityWithAnim
import dev.byto.tmdb.databinding.ActivitySplashBinding
import dev.byto.tmdb.ui.MainActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {

    private val viewBinding: ActivitySplashBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setAnimation()
    }

    private fun setAnimation() {
        Handler(Looper.getMainLooper()).postDelayed({
            startMainActivity()
        }, 3000)
    }

    private fun startMainActivity() {
        finish()
        startActivityWithAnim(MainActivity::class.java, animIn = 0, animOut = 0)
    }
}