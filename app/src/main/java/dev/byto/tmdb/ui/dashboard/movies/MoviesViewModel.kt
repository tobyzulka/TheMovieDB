package dev.byto.tmdb.ui.dashboard.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.repository.MovieRepository
import dev.byto.tmdb.data.repository.TrendingRepository
import dev.byto.tmdb.ui.dashboard.datasources.MoviePagingDataSource
import dev.byto.tmdb.ui.dashboard.datasources.TrendingMoviesPagingDataSource

class MoviesViewModel(
    private val movieRepository: MovieRepository,
    private val trendingRepository: TrendingRepository
) : ViewModel() {

    /**
     * Popular movies
     */
    private var nowPlayingDataSource: MoviePagingDataSource? = null

    var nowPlayingMoviesFlow = Pager(PagingConfig(pageSize = 20)) {
        MoviePagingDataSource(movieRepository, MovieCollectionType.NOW_PLAYING).also {
            nowPlayingDataSource = it
        }
    }.flow.cachedIn(viewModelScope)

    /**
     * Upcoming movies
     */
    private var upcomingDataSource: MoviePagingDataSource? = null

    var upcomingMoviesFlow = Pager(PagingConfig(pageSize = 20)) {
        MoviePagingDataSource(movieRepository, MovieCollectionType.UPCOMING).also {
            upcomingDataSource = it
        }
    }.flow.cachedIn(viewModelScope)

    /**
     * Upcoming movies
     */
    private var trendingDataSource: TrendingMoviesPagingDataSource? = null

    var trendingMoviesFlow = Pager(PagingConfig(pageSize = 20)) {
        TrendingMoviesPagingDataSource(trendingRepository, TrendingRepository.TimeWindow.WEEK, AppConfig.REGION).also {
            trendingDataSource = it
        }
    }.flow.cachedIn(viewModelScope)

    /**
     * Upcoming movies
     */
    private var popularDataSource: MoviePagingDataSource? = null

    var popularMoviesFlow = Pager(PagingConfig(pageSize = 20)) {
        MoviePagingDataSource(movieRepository, MovieCollectionType.POPULAR).also {
            popularDataSource = it
        }
    }.flow.cachedIn(viewModelScope)

    /**
     * One-shot get genres list
     */
    val genresLiveData = liveData {
        emit(movieRepository.getMovieGenres(AppConfig.REGION))
    }
}