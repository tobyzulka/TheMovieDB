package dev.byto.tmdb.ui.dashboard.datasources

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.orhanobut.logger.Logger
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.TrendingRepository

class TrendingMoviesPagingDataSource(
    private val repository: TrendingRepository,
    private val timeWindow: TrendingRepository.TimeWindow,
    private val language: String?
) : PagingSource<Int, Movie>() {

    private var totalPages: Int? = null

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? = null

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Movie> {
        // Start refresh at page 1 if undefined.
        val nextPageNumber = params.key ?: 1

        if (totalPages != null && nextPageNumber > totalPages!!) {
            return LoadResult.Error(Exception("Max page. nextPage: $nextPageNumber, maxPage: $totalPages"))
        }

        return when (val response = repository.getTrendingMovies(timeWindow, nextPageNumber, language)) {
            is ResultWrapper.GenericError -> {
                Logger.e("Generic Error")

                LoadResult.Error(
                    Exception(
                        "Generic Error  ${response.error?.statusMessage} ${response.error?.statusCode}"
                    )
                )
            }

            is ResultWrapper.NetworkError -> {
                Logger.e("Network Error")
                LoadResult.Error(Exception("Network Error}"))
            }

            is ResultWrapper.Success -> {
                totalPages = response.value.totalPages

                LoadResult.Page(
                    data = response.value.results,
                    prevKey = null, // Only paging forward.
                    nextKey = nextPageNumber + 1
                )
            }
        }
    }
}
