package dev.byto.tmdb.ui.search

internal interface SearchQueryChangeListener {
    fun queryChange(query: String)
}
