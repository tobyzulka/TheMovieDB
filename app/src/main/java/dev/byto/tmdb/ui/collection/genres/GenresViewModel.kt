package dev.byto.tmdb.ui.collection.genres

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.models.network.GenreResponse
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.MovieRepository
import kotlinx.coroutines.launch

class GenresViewModel(
    private val movieRepository: MovieRepository
) : ViewModel() {

    private var genresLiveData = MutableLiveData<GenreResponse?>()

    fun fetchGenres() {
        viewModelScope.launch {

            when (val response = movieRepository.getMovieGenres(AppConfig.REGION)) {
                is ResultWrapper.NetworkError -> {
                }

                is ResultWrapper.GenericError -> {
                }

                is ResultWrapper.Success -> {
                    genresLiveData.postValue(response.value)
                }
            }
        }
    }
}