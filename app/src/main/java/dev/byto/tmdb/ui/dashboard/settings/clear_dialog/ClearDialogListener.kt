package dev.byto.tmdb.ui.dashboard.settings.clear_dialog

interface ClearDialogListener {
    fun onDialogDismiss()
}
