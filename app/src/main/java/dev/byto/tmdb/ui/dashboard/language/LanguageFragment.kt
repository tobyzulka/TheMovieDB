package dev.byto.tmdb.ui.dashboard.language

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.updateLanguage
import dev.byto.tmdb.databinding.FragmentLanguageBinding
import dev.byto.tmdb.ui.dashboard.adapters.LanguageAdapter

class LanguageFragment : Fragment(R.layout.fragment_language) {

    private val viewBinding: FragmentLanguageBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureAdapter()
        setClickListener()
    }

    private fun configureAdapter() {
        viewBinding.listLanguages.adapter = LanguageAdapter {
            (activity as? LocalizationActivity)?.updateLanguage(it)
        }

        viewBinding.listLanguages.setHasFixedSize(true)
    }

    private fun setClickListener() {
        viewBinding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}