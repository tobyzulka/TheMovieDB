package dev.byto.tmdb.ui.dashboard.settings

import android.app.usage.StorageStatsManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageStats
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Process
import android.os.storage.StorageManager
import android.text.format.Formatter
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.orhanobut.logger.Logger
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.getCurrentLocale
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.databinding.FragmentSettingBinding
import dev.byto.tmdb.ui.dashboard.settings.clear_dialog.ClearDialogListener

class SettingFragment : Fragment(R.layout.fragment_setting), ClearDialogListener {

    private val viewBinding: FragmentSettingBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calculateCache()

        setClickListeners()
        setCurrentLanguage()
    }

    override fun onResume() {
        super.onResume()

        viewBinding.settingsCacheSize.text = Formatter.formatShortFileSize(context, calculateCache())
    }

    override fun onDialogDismiss() {
        viewBinding.settingsCacheSize.text = Formatter.formatShortFileSize(context, calculateCache())
    }

    private fun setClickListeners() {
        viewBinding.btnClearCache.setSafeOnClickListener {
            findNavController().navigate(SettingFragmentDirections.actionToClearCache(viewBinding.settingsCacheSize.text.toString()))
        }

        viewBinding.btnChangeLang.setSafeOnClickListener {
            findNavController().navigate(SettingFragmentDirections.actionToChangeLanguage())
        }
        viewBinding.cvAbout.setSafeOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://www.linkedin.com/in/tobyzulka/")
            startActivity(intent)
        }

        viewBinding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setCurrentLanguage() {
        val currentLanguage = (activity as? LocalizationActivity)?.getCurrentLanguage() ?: context?.getCurrentLocale()
        viewBinding.tvCurrentLanguage.text =
            currentLanguage?.getDisplayName(currentLanguage)?.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(
                    currentLanguage
                ) else it.toString()
            } ?: ""
    }

    private fun calculateCache(): Long {
        if (activity == null) return 0L

        val cacheSize: Long = if (Build.VERSION.SDK_INT >= 26) {
            val ssm = requireActivity().getSystemService(Context.STORAGE_STATS_SERVICE) as? StorageStatsManager
            val user = Process.myUserHandle()

            val sm = ssm?.queryStatsForPackage(StorageManager.UUID_DEFAULT, requireActivity().packageName, user)
            sm?.cacheBytes ?: 0L
        } else {
            val p = PackageStats(requireActivity().packageName)
            p.cacheSize
        }

        Logger.i("getCacheBytes ${Formatter.formatShortFileSize(context, cacheSize)}")

        return cacheSize
    }
}