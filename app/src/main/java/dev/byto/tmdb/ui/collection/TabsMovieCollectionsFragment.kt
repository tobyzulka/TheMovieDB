package dev.byto.tmdb.ui.collection

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import dev.byto.tmdb.R
import dev.byto.tmdb.base.enums.MovieCollectionType
import dev.byto.tmdb.databinding.FragmentTabCollectionsBinding
import dev.byto.tmdb.ui.collection.adapters.FragmentsPagerAdapter

class TabsMovieCollectionsFragment : Fragment(R.layout.fragment_tab_collections) {

    private val viewBinding: FragmentTabCollectionsBinding by viewBinding()

    private val args: TabsMovieCollectionsFragmentArgs by navArgs()

    private lateinit var pagerAdapter: FragmentsPagerAdapter

    private val fragments = listOf<Fragment>(
        CollectionFragment(MovieCollectionType.NOW_PLAYING, null),
        CollectionFragment(MovieCollectionType.POPULAR, null),
        CollectionFragment(MovieCollectionType.TOP_RATED, null),
        CollectionFragment(MovieCollectionType.UPCOMING, null)
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureTabLayout()
        setPage(args.movieCollectionType)
        setClickListener()
    }

    private fun setClickListener() {
        viewBinding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setPage(collectionName: MovieCollectionType) {
        when (collectionName) {
            MovieCollectionType.NOW_PLAYING -> {
                viewBinding.collectionViewPager.setCurrentItem(0, true)
            }

            MovieCollectionType.POPULAR -> {
                viewBinding.collectionViewPager.setCurrentItem(1, true)
            }

            MovieCollectionType.TOP_RATED -> {
                viewBinding.collectionViewPager.setCurrentItem(2, true)
            }

            MovieCollectionType.UPCOMING -> {
                viewBinding.collectionViewPager.setCurrentItem(3, true)
            }
        }
    }

    private fun configureTabLayout() {
        val pagerTitles: Array<String> =
            arrayOf(
                getString(R.string.collections_now_playing),
                getString(R.string.popular),
                getString(R.string.collections_collection_top_rated),
                getString(R.string.upcoming)
            )

        pagerAdapter = FragmentsPagerAdapter(
            fragments,
            childFragmentManager,
            lifecycle
        )

        viewBinding.collectionViewPager.adapter = pagerAdapter
        viewBinding.collectionViewPager.offscreenPageLimit = fragments.size

        TabLayoutMediator(viewBinding.pTabLayout, viewBinding.collectionViewPager) { tab, position ->
            tab.text = pagerTitles.getOrNull(position)
        }.attach()
    }
}
