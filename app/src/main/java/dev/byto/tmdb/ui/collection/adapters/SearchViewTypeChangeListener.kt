package dev.byto.tmdb.ui.collection.adapters

interface SearchViewTypeChangeListener {
    fun changeViewType(isGrid: Boolean)
}
