package dev.byto.tmdb.ui.search.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.orhanobut.logger.Logger
import dev.byto.tmdb.constants.AppConfig
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.SearchRepository

class SearchMoviePagingDataSource(
    private val query: String,
    private val repository: SearchRepository
) : PagingSource<Int, Movie>() {

    private var totalPages: Int? = null

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Movie> {
        // Start refresh at page 1 if undefined.
        val nextPageNumber = params.key ?: 1

        if (totalPages != null && nextPageNumber > totalPages!!) {
            return LoadResult.Error(Exception("Max page. nextPage: $nextPageNumber, maxPage: $totalPages"))
        }

        return when (
            val response =
                repository.searchMovies(
                    AppConfig.REGION, query, nextPageNumber, false, null, null, null
                )
        ) {
            is ResultWrapper.GenericError -> {
                Logger.e("Generic Error")

                LoadResult.Error(
                    Exception(
                        "Generic Error  ${response.error?.statusMessage} ${response.error?.statusCode}"
                    )
                )
            }

            is ResultWrapper.NetworkError -> {
                Logger.e("Network Error")
                LoadResult.Error(Exception("Network Error}"))
            }

            is ResultWrapper.Success -> {
                totalPages = response.value.totalPages

                LoadResult.Page(
                    data = response.value.results,
                    prevKey = null, // Only paging forward.
                    nextKey = nextPageNumber + 1
                )
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? = null
}
