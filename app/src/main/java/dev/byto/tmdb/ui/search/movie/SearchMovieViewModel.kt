package dev.byto.tmdb.ui.search.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dev.byto.tmdb.data.repository.SearchRepository
import dev.byto.tmdb.ui.search.datasource.SearchMoviePagingDataSource
import kotlinx.coroutines.flow.MutableStateFlow

class SearchMovieViewModel (
    repository: SearchRepository
) : ViewModel() {

    val query = MutableStateFlow("")

    var searchMoviesDataSource: SearchMoviePagingDataSource? = null

    var searchMoviesFlow = Pager(PagingConfig(pageSize = 20)) {
        SearchMoviePagingDataSource(query.value, repository).also {
            searchMoviesDataSource = it
        }
    }.flow.cachedIn(viewModelScope)
}
