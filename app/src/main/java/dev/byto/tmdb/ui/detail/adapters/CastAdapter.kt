package dev.byto.tmdb.ui.detail.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.displayImageWithCenterInside
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Cast
import dev.byto.tmdb.databinding.ItemCastProfileCardDetailsBinding
import dev.byto.tmdb.ui.detail.adapters.CastAdapter.CastViewHolder

class CastAdapter(private val people: List<Cast>) :
    RecyclerView.Adapter<CastViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemCastProfileCardDetailsBinding.inflate(layoutInflater, parent, false)

        return CastViewHolder(viewBinding)
    }

    override fun getItemCount() = people.size

    override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
        holder.bindTo(people[position])
    }

    class CastViewHolder(val viewBinding: ItemCastProfileCardDetailsBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bindTo(cast: Cast) {
            viewBinding.personName.text = cast.name
            viewBinding.characterName.text = cast.character
            viewBinding.personProfileImage.displayImageWithCenterInside(
                UrlConstants.TMDB_PROFILE_SIZE_185 + cast.profilePath
            )
        }
    }
}
