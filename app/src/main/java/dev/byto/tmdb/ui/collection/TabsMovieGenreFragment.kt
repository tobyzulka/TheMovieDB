package dev.byto.tmdb.ui.collection

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import dev.byto.tmdb.R
import dev.byto.tmdb.databinding.FragmentTabGenreBinding
import dev.byto.tmdb.ui.collection.adapters.FragmentsGenrePagerAdapter

class TabsMovieGenreFragment : Fragment(R.layout.fragment_tab_genre) {

    private val viewBinding: FragmentTabGenreBinding by viewBinding()

    private val args: TabsMovieGenreFragmentArgs by navArgs()

    private lateinit var pagerAdapter: FragmentsGenrePagerAdapter

    private val fragments = CollectionFragment(null, args.name)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setClickListener()
    }

    private fun setClickListener() {
        viewBinding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }


    private fun configGenreTabLayout(name: String) {
        pagerAdapter = FragmentsGenrePagerAdapter(
            childFragmentManager,
            lifecycle
        )
        pagerAdapter.addFragment(fragments, name)

//        viewBinding.collectionViewPager.adapter = pagerAdapter

//        TabLayoutMediator(viewBinding.pTabLayout, viewBinding.collectionViewPager) { tab, position ->
//            tab.text = pagerAdapter.getPageTitle(position)
//            viewBinding.collectionViewPager.setCurrentItem(tab.position, true)
//        }.attach()
    }
}
