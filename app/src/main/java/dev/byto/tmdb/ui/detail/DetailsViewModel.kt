package dev.byto.tmdb.ui.detail

import androidx.lifecycle.viewModelScope
import dev.byto.tmdb.base.viewmodel.BaseAction
import dev.byto.tmdb.base.viewmodel.BaseViewModelFlow
import dev.byto.tmdb.base.viewmodel.BaseViewState
import dev.byto.tmdb.data.models.entity.MovieDetails
import dev.byto.tmdb.data.models.result.ResultWrapper
import dev.byto.tmdb.data.repository.MovieRepository
import kotlinx.coroutines.launch

internal class DetailsViewModel(
    private val movieRepository: MovieRepository,
) : BaseViewModelFlow<DetailsViewModel.ViewState, DetailsViewModel.Action>(ViewState()) {

    fun fetchMovieDetails(
        movieId: Int,
        language: String?,
        appendToResponse: String?
    ) {
        sendAction(Action.ContentIsLoading)
        viewModelScope.launch {
            val response =
                movieRepository.getMovieById(movieId, language, appendToResponse)

            when (response) {
                is ResultWrapper.NetworkError -> sendAction(Action.NetworkError)
                is ResultWrapper.GenericError -> sendAction(Action.ContentLoadFailure)
                is ResultWrapper.Success -> {
                    sendAction(Action.ContentLoadSuccess(response.value))
                }
            }
        }
    }

    /**
     *ViewState
     *
     * @param isContentLoaded - when to update details
     *
     * [isContentLoaded] - when to update details
     * [details] - movie details
     * [isLoading] - when some data is loaded
     * [isError] - when an error occurred
     * [networkError] - when a network error occurred (for example: the Internet disappeared)
     */
    internal data
    class ViewState(
        val isContentLoaded: Boolean? = false,
        val details: MovieDetails? = null,
        val isLoading: Boolean = true,
        val isError: Boolean = false,
        val networkError: Boolean = false
    ) : BaseViewState

    /**
     * Actions
     */
    internal sealed class Action : BaseAction {
        data object ContentIsLoading : Action() // while the content is loading
        data class ContentLoadSuccess(val details: MovieDetails) : Action()
        data object ContentLoadFailure : Action()
        data object NetworkError : Action() // if there is no internet or other network error
    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.ContentIsLoading -> state.copy(
            isLoading = true,
            isContentLoaded = false,
            isError = false,
            networkError = false,
        )

        is Action.ContentLoadSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isContentLoaded = true,
            networkError = false,
            details = viewAction.details
        )

        is Action.ContentLoadFailure -> state.copy(
            isLoading = false,
            isError = true,
            isContentLoaded = false,
            networkError = false,
        )

        is Action.NetworkError -> state.copy(
            isLoading = false,
            isContentLoaded = false,
            isError = true,
            networkError = true
        )
    }
}