package dev.byto.tmdb.ui.detail.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.data.models.entity.Review
import dev.byto.tmdb.databinding.ItemReviewCardDetailsBinding
import dev.byto.tmdb.ui.detail.adapters.ReviewAdapter.ReviewViewHolder
import dev.byto.tmdb.utils.DateTimeUtil

class ReviewAdapter(private val review: List<Review>) :
    RecyclerView.Adapter<ReviewViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemReviewCardDetailsBinding.inflate(layoutInflater, parent, false)

        return ReviewViewHolder(viewBinding)
    }

    override fun getItemCount() = review.size

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bindTo(review[position])
    }

    class ReviewViewHolder(val viewBinding: ItemReviewCardDetailsBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bindTo(review: Review) {
            viewBinding.cardTitle.text = review.author
            viewBinding.cardRate.text = (review.authorDetails.rating ?: 0).toString()
            val date = DateTimeUtil.formatStringDate(review.updateAt, "MMM dd, yyyy")
            val writer = "Written by ${review.author} on $date"
            viewBinding.cardWriter.text = writer
            viewBinding.cardContent.text = review.content
        }
    }
}