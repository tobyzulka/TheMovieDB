package dev.byto.tmdb.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.data.models.entity.Genre
import dev.byto.tmdb.databinding.ItemGenreBinding
import dev.byto.tmdb.ui.dashboard.adapters.GenreAdapter.GenreViewHolder
import java.util.*

class GenreAdapter(private val clickActionGenre: (id: Int) -> Unit) :
    RecyclerView.Adapter<GenreViewHolder>() {

    private val genres: MutableList<Genre> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemGenreBinding.inflate(layoutInflater, parent, false)

        return GenreViewHolder(binding)
    }

    override fun getItemCount() = genres.size

    override fun getItemId(position: Int): Long {
        return genres[position].id.toLong()
    }

    fun addItems(items: List<Genre>) {
        items.forEach {
            addItem(it)
        }
    }

    private fun addItem(item: Genre) {
        val startedPosition = genres.size
        genres.add(item)
        notifyItemInserted(startedPosition)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        holder.bindTo(genres[position])
        holder.itemView.setSafeOnClickListener {
            clickActionGenre(genres[position].id)
            notifyDataSetChanged()
        }
    }

    class GenreViewHolder(private val viewBinding: ItemGenreBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(genre: Genre) {
            viewBinding.genreName.text = genre.name.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(
                    Locale.getDefault()
                ) else it.toString()
            }
        }
    }
}
