package dev.byto.tmdb.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.base.extensions.toDate
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemTrendCardWithTitleBinding
import dev.byto.tmdb.ui.dashboard.adapters.MovieTrendAdapter.MovieTrendViewHolder
import dev.byto.tmdb.utils.GenresStorageObject
import dev.byto.tmdb.utils.getMovieDiffUtils
import kotlin.math.roundToInt

class MovieTrendAdapter(private val clickAction: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, MovieTrendViewHolder> (getMovieDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieTrendViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemTrendCardWithTitleBinding.inflate(layoutInflater, parent, false)

        return MovieTrendViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieTrendViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.bindTo(movie)

            holder.viewBinding.root.setSafeOnClickListener {
                clickAction(movie.id)
            }
        }
    }

    class MovieTrendViewHolder(val viewBinding: ItemTrendCardWithTitleBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bindTo(movie: Movie) {
            viewBinding.mTrendTitle.text = movie.title

            viewBinding.mTrendYear.text = movie.releaseDate?.toDate()?.yearInt?.toString() ?: ""
            viewBinding.mTrendGenres.text =
                movie.genreIds.map { GenresStorageObject.movieGenres.get(it) }
                    .take(2)
                    .joinToString(" - ") { it ?: "" }

            val rate = "${(movie.voteAverage * 10).roundToInt()}%"
            viewBinding.mTrendRating.text = rate
        }
    }
}
