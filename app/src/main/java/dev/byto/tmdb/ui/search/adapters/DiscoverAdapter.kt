package dev.byto.tmdb.ui.search.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.R
import dev.byto.tmdb.base.enums.DiscoverType
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setIndicatorColor
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.BaseDiscoverDomainModel
import dev.byto.tmdb.databinding.ItemCardWithDetailsBinding
import dev.byto.tmdb.databinding.ItemMediumPosterCardBinding
import dev.byto.tmdb.utils.GenresStorageObject
import dev.byto.tmdb.utils.getDiscoverDiffUtils
import java.util.Locale

class DiscoverAdapter(
    private val changeItemTypeAction: (isGrid: Boolean) -> Unit,
    private val actionClick: (Int, DiscoverType) -> Unit
) : PagingDataAdapter<BaseDiscoverDomainModel, RecyclerView.ViewHolder> (getDiscoverDiffUtils()) {

    // If you want the elements to be displayed as 3x3 cards, otherwise they will be displayed as long cards
    // with additional information
    private var isGrid: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            R.layout.item_medium_poster_card -> {
                val view = ItemMediumPosterCardBinding.inflate(layoutInflater, parent, false)
                DiscoverCardViewHolder(view)
            }

            R.layout.item_card_with_details -> {
                val view = ItemCardWithDetailsBinding.inflate(layoutInflater, parent, false)
                DiscoverDetailsCardViewHolder(view)
            }

            else -> throw IllegalArgumentException("Unknown view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { item ->
            when (holder) {
                is DiscoverCardViewHolder -> {
                    holder.bindTo(item)

                    holder.viewBinding.collectionCard.setSafeOnClickListener {
                        actionClick(item.id, item.type)
                    }
                }

                is DiscoverDetailsCardViewHolder -> {
                    holder.bindTo(item)

                    holder.viewBinding.root.setSafeOnClickListener {
                        actionClick(item.id, item.type)
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isGrid) {
            R.layout.item_medium_poster_card
        } else {
            R.layout.item_card_with_details
        }
    }

    /**
     * [setViewType] method for changing cell type
     */
    fun setViewType(isGrid: Boolean) {
        this.isGrid = isGrid

        changeItemTypeAction(isGrid)
    }

    class DiscoverCardViewHolder(val viewBinding: ItemMediumPosterCardBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(item: BaseDiscoverDomainModel) {
            viewBinding.collectionImage.displayImageWithCenterCrop(UrlConstants.TMDB_POSTER_SIZE_185 + item.posterImage)
            viewBinding.title.text = item.name
        }
    }

    class DiscoverDetailsCardViewHolder(val viewBinding: ItemCardWithDetailsBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(item: BaseDiscoverDomainModel) {
            viewBinding.cardImage.displayImageWithCenterCrop(UrlConstants.TMDB_BACKDROP_SIZE_1280 + item.backdropImage)
            viewBinding.cardTitle.text = item.name

            item.releaseDate?.let {
                viewBinding.cardReleaseDate.text = it.yearInt.toString()
            }

            viewBinding.cardVoteAverage.text = item.voteAverage.toString()

            viewBinding.cardGenres.text = item.genres?.map { GenresStorageObject.movieGenres.get(it) }
                ?.joinToString(" - ") { it ->
                    (it ?: "").replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(
                        Locale.getDefault()
                    ) else it.toString()
                } }

            viewBinding.voteAverageIndicator.setIndicatorColor(item.voteAverage)
        }
    }
}
