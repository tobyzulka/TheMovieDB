package dev.byto.tmdb.ui.search.movie

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.px
import dev.byto.tmdb.base.extensions.setAdapterWithFixedSize
import dev.byto.tmdb.databinding.FragmentSearchMovieBinding
import dev.byto.tmdb.ui.search.SearchFragmentDirections
import dev.byto.tmdb.ui.search.SearchQueryChangeListener
import dev.byto.tmdb.ui.search.SearchViewTypeChangeListener
import dev.byto.tmdb.ui.search.adapters.SearchMoviePagingAdapter
import dev.byto.tmdb.utils.SpacingDecoration
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchMovieFragment : Fragment(R.layout.fragment_search_movie),
    SearchQueryChangeListener, SearchViewTypeChangeListener {

    private val viewBinding: FragmentSearchMovieBinding by viewBinding()

    private val viewModel: SearchMovieViewModel by viewModel()

    private var adapter: SearchMoviePagingAdapter = SearchMoviePagingAdapter { id ->
        findNavController().navigate(SearchFragmentDirections.actionToMovieDetails(id))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()
        configureObservables()
    }

    override fun queryChange(query: String) {
        lifecycleScope.launchWhenResumed {
            adapter.submitData(PagingData.empty())
            viewModel.query.value = query
            viewModel.searchMoviesDataSource?.invalidate()
        }
    }

    override fun changeViewType(isGrid: Boolean) {
        if (isGrid) {
            viewBinding.searchList.layoutManager = GridLayoutManager(context, 3)
        } else {
            viewBinding.searchList.layoutManager = GridLayoutManager(context, 1)
        }

        adapter.setViewType(isGrid)
    }

    private fun configureRecyclerView() {
        viewBinding.searchList.setAdapterWithFixedSize(ScaleInAnimationAdapter(adapter), true)
        viewBinding.searchList.addItemDecoration(SpacingDecoration(16.px(), 16.px(), true))
    }

    private fun configureObservables() {
        lifecycleScope.launchWhenResumed {
            viewModel.searchMoviesFlow.collectLatest { pagingData ->
                adapter.submitData(pagingData)
            }
        }
    }
}