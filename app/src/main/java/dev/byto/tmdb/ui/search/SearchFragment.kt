package dev.byto.tmdb.ui.search

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.setVisibilityOption
import dev.byto.tmdb.base.extensions.onQueryTextChange
import dev.byto.tmdb.databinding.FragmentSearchBinding
import dev.byto.tmdb.ui.search.adapters.FragmentsPagerAdapter
import dev.byto.tmdb.ui.search.movie.SearchMovieFragment

class SearchFragment : Fragment(R.layout.fragment_search) {

    private val viewBinding: FragmentSearchBinding by viewBinding()

    private var pagerAdapter: FragmentsPagerAdapter? = null

    private val fragments =
        listOf<Fragment>(
            SearchMovieFragment()
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureFloatingSearchView()
        configureTabLayout()
        setClickListener()
    }

    private fun setClickListener() {
        viewBinding.apply {
            viewBinding.btnBack.setOnClickListener {
                findNavController().popBackStack()
            }

            toggleIsGridType.setOnCheckedChangeListener { _, isChecked ->
                pagerAdapter?.changeViewType(isChecked)
            }
        }
    }

    private fun configureTabLayout() {
        pagerAdapter = FragmentsPagerAdapter(
            fragments,
            childFragmentManager,
            lifecycle
        )
        viewBinding.pViewPager.adapter = pagerAdapter
        viewBinding.pViewPager.offscreenPageLimit = 1

    }

    private fun configureFloatingSearchView() {
        viewBinding.searchView.requestFocus()

        viewBinding.searchView.onQueryTextChange(lifecycle) { query ->
            fragments.forEach {
                if (it is SearchQueryChangeListener) {
                    it.queryChange(query)
                }
            }
        }

        viewBinding.searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            viewBinding.toggleIsGridType.setVisibilityOption(hasFocus)
        }
    }
}