package dev.byto.tmdb.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.byto.tmdb.base.extensions.displayImageWithCenterCrop
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.constants.UrlConstants
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemBigImageWithCornersBinding
import dev.byto.tmdb.utils.getMovieDiffUtils
import dev.byto.tmdb.ui.dashboard.adapters.MovieCardAdapter.MovieCardViewHolder

class MovieCardAdapter(private val clickAction: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, MovieCardViewHolder>(
        getMovieDiffUtils()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieCardViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemBigImageWithCornersBinding.inflate(layoutInflater, parent, false)

        return MovieCardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieCardViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.bindTo(movie)

            holder.viewBinding.sliderLayout.setSafeOnClickListener {
                clickAction(movie.id)
            }
        }
    }

    class MovieCardViewHolder(val viewBinding: ItemBigImageWithCornersBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindTo(movie: Movie) {
            viewBinding.placeholderText.text = movie.title

            viewBinding.sliderImage.displayImageWithCenterCrop(
                UrlConstants.TMDB_BACKDROP_SIZE_1280 + movie.backdropPath
            )
        }
    }
}
