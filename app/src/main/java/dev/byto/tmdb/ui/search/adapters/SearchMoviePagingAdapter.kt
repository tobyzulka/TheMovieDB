package dev.byto.tmdb.ui.search.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.data.models.entity.Movie
import dev.byto.tmdb.databinding.ItemCardWithDetailsBinding
import dev.byto.tmdb.databinding.ItemMediumPosterCardBinding
import dev.byto.tmdb.ui.search.movie.SearchMovieDetailedVH
import dev.byto.tmdb.ui.search.movie.SearchMovieSmallVH
import dev.byto.tmdb.utils.getMovieDiffUtils

internal class SearchMoviePagingAdapter(private val actionClick: (id: Int) -> Unit) :
    PagingDataAdapter<Movie, ViewHolder> (getMovieDiffUtils()) {

    private var isGrid: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            R.layout.item_card_with_details -> SearchMovieDetailedVH(
                ItemCardWithDetailsBinding.inflate(layoutInflater, parent, false)
            )

            R.layout.item_medium_poster_card -> SearchMovieSmallVH(
                ItemMediumPosterCardBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
            )

            else -> throw IllegalArgumentException("Unknown view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position) ?: return

        when (holder) {
            is SearchMovieDetailedVH -> {
                holder.bindTo(item)

                holder.viewBinding.root.setSafeOnClickListener {
                    actionClick(item.id)
                }
            }

            is SearchMovieSmallVH -> {
                holder.bindTo(item)

                holder.viewBinding.collectionCard.setSafeOnClickListener {
                    actionClick(item.id)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isGrid) {
            R.layout.item_medium_poster_card
        } else {
            R.layout.item_card_with_details
        }
    }

    fun setViewType(isGrid: Boolean) {
        this.isGrid = isGrid
    }
}
