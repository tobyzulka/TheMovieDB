package dev.byto.tmdb.ui.dashboard.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.setSafeOnClickListener
import dev.byto.tmdb.databinding.FragmentSearchDashboardBinding

class SearchDashboardFragment : Fragment(R.layout.fragment_search_dashboard) {

    private val viewBinding: FragmentSearchDashboardBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.searchBar.setSafeOnClickListener {
            findNavController().navigate(SearchDashboardFragmentDirections.actionToSearch())
        }

        viewBinding.btnDiscover.setSafeOnClickListener {
            findNavController().navigate(SearchDashboardFragmentDirections.actionToDiscover())
        }
    }
}