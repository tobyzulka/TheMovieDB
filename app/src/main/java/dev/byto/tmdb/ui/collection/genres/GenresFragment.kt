package dev.byto.tmdb.ui.collection.genres

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dev.byto.tmdb.R
import dev.byto.tmdb.base.extensions.setAdapterWithFixedSize
import dev.byto.tmdb.databinding.FragmentGenresBinding
import dev.byto.tmdb.ui.collection.adapters.GenresInlineAdapter
import dev.byto.tmdb.ui.dashboard.adapters.MovieCardAdapter
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class GenresFragment : Fragment(R.layout.fragment_genres) {

    private val viewBinding: FragmentGenresBinding by viewBinding()

    private val viewModel: GenresViewModel by viewModel()

    private val adapters = GenresInlineAdapter(::clickAction)

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        var anim: Animation? = super.onCreateAnimation(transit, enter, nextAnim)

        if (anim == null && nextAnim != 0) {
            anim = AnimationUtils.loadAnimation(context, nextAnim)
        }

        anim?.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) = Unit

            override fun onAnimationEnd(animation: Animation?) {
                openFiltersDialogByDefault()
            }

            override fun onAnimationRepeat(animation: Animation?) = Unit
        })

        return anim
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapters()
        setObservers()

        viewBinding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun openFiltersDialogByDefault() {
        lifecycleScope.launchWhenResumed {
            delay(150)
        }
    }

    private fun setObservers() {
        viewModel.run {
            lifecycleScope.launchWhenResumed {
                fetchGenres()
            }
        }
    }

    private fun initAdapters() {
        lifecycleScope.launchWhenResumed {
            viewBinding.rvGenres.adapter = adapters
        }
    }

    /**
     * Actions
     */
    private fun clickAction(name: String) {
        findNavController().navigate(GenresFragmentDirections.actionToMovieGenre(name))
    }
}