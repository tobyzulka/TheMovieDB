import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    namespace = "dev.byto.tmdb"
    compileSdk = 34

    defaultConfig {
        applicationId = "dev.byto.tmdb"
        minSdk = 26
        targetSdk = 33
        versionCode = 10000
        versionName = "1.0.0"

        val key = gradleLocalProperties(rootDir).getProperty("API_KEY")
        buildConfigField(type = "String", name = "API_KEY", value = key)

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        multiDexEnabled = true
    }

    buildTypes {
        debug {
            isMinifyEnabled = false
        }
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        viewBinding = true
        buildConfig = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
}

dependencies {

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    val multidexVersion = "2.0.1"
    val lifeCycleExtVersion = "2.6.2"
    val retrofitVersion = "2.9.0"
    val okhttpVersion = "4.11.0"
    val moshiVersion = "1.14.0"
    val coroutineVersion = "1.7.3"
    val navigationVersion = "2.7.2"
    val koinVersion = "3.4.3"
    val roomVersion = "2.2.5"

    // AndroidX
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.annotation:annotation:1.7.0")
    implementation("androidx.fragment:fragment-ktx:1.6.1")
    implementation("androidx.activity:activity-ktx:1.7.2")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.coordinatorlayout:coordinatorlayout:1.2.0")
    implementation("androidx.paging:paging-runtime-ktx:3.2.1")
    implementation("androidx.recyclerview:recyclerview:1.3.1")
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("androidx.multidex:multidex:$multidexVersion")

    // Room
    implementation("androidx.room:room-runtime:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.room:room-ktx:$roomVersion")

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.9.10")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.9.10")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.7.3")

    // Google
    implementation("com.google.android.material:material:1.9.0")

    // Koin DI
    implementation("io.insert-koin:koin-android:$koinVersion")

    // Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutineVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutineVersion")

    // Reactive Extensions (RX)
    implementation("io.reactivex.rxjava3:rxandroid:3.0.2")
    implementation("io.reactivex.rxjava3:rxjava:3.1.7")

    // ViewModel Lifecycle Scopes
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifeCycleExtVersion")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifeCycleExtVersion")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifeCycleExtVersion")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")

    // Navigation
    implementation("androidx.navigation:navigation-fragment-ktx:$navigationVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$navigationVersion")
    implementation("androidx.navigation:navigation-dynamic-features-fragment:$navigationVersion")

    // Retrofit
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-gson:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")

    implementation("com.github.akarnokd:rxjava3-retrofit-adapter:3.0.0")
    implementation("com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:0.9.2")

    // OkHttp3
    implementation("com.squareup.okhttp3:okhttp:$okhttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpVersion")

    //  Moshi
    implementation("com.squareup.moshi:moshi:$moshiVersion")
    implementation("com.squareup.moshi:moshi-kotlin:$moshiVersion")

    // Third Party Library
    implementation("com.akexorcist:localization:1.2.11")
    implementation("io.coil-kt:coil:2.4.0")
    implementation("com.soywiz.korlibs.klock:klock-android:4.0.2")
    implementation("jp.wasabeef:recyclerview-animators:4.0.2")
    implementation("com.github.stfalcon-studio:StfalconImageViewer:1.0.1")
    implementation("com.github.kirich1409:viewbindingpropertydelegate-full:1.5.9")
    implementation("com.github.yuzumone:ExpandableTextView:0.3.2")
    implementation("com.github.Jay-Goo:RangeSeekBar:3.0.0")

    // Logger
    implementation("com.orhanobut:logger:2.2.0")
    implementation("com.jakewharton.timber:timber:5.0.1")

    // Testing
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    constraints{
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.10")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.10")
    }
}